angular.module('starter.controllers', [])


app.controller('AppCtrl', function($scope, $state, $ionicModal, $cordovaCamera, $cordovaCapture, $ionicActionSheet,
  $firebaseArray,$firebaseObject, $timeout, $ionicModal, $ionicScrollDelegate, $filter,$cordovaGeolocation, ProductDetail, $ionicNavBarDelegate) {

  $scope.images = [];
  $scope.product = {};

  var allref = new Firebase("https://kidsify.firebaseio.com/products");
  $scope.allproducts = $firebaseArray(allref);

  var refUsers = new Firebase("https://kidsify.firebaseio.com/users");
  var refUser = new Firebase("https://kidsify.firebaseio.com/users").child(window.localStorage.getItem('uid'));

  $scope.$on('$ionicView.enter', function(e) {
    $scope.images = [];
    $scope.product = {};

    refUser = new Firebase("https://kidsify.firebaseio.com/users").child(window.localStorage.getItem('uid'));
    $scope.user = $firebaseObject(refUser);
  });

  $scope.user = $firebaseObject(refUser);

  //LOGOUT FAKE
  $scope.logOut = function() {
    window.localStorage.removeItem("uid");
    window.localStorage.removeItem("pushUid");

    refUsers.unauth();

    $state.go('login');
  }

  //Open create Modal
  $scope.dragup = false;
  $scope.dragdown = false;
  $scope.openCreateModal = function() {
    $ionicModal.fromTemplateUrl('templates/createproductmodal.html', {
      scope: $scope,
      animation: 'slide-in-down'
    }).then(function(modal) {
      $scope.modal = modal;
      $scope.modal.show();
    });
  }

  $scope.canUpdate = function() {
    //if($scope.images.length==0) return false;
    // if($scope.videoData.length==0) return false;
    if(!$scope.product.name) return false;
    if(!$scope.product.description) return false;
    if(!$scope.product.price) return false;

    if(!$scope.generoSelected) return false;
    if($scope.generoSelected == 'Niño' || $scope.generoSelected == 'Niña'){
      if(!$scope.edadSelected) return false;
    }
    if($scope.generoSelected == 'Bebe'){
      if(!$scope.mesSelected) return false;
    }

    if(!$scope.articuloSelected) return false;
    if($scope.articuloSelected == 'Ropa') {
      if(!$scope.ropaSelected ) return false;
    }
    if($scope.articuloSelected == 'Calzado') {
      if(!$scope.calzadoSelected ) return false;
    }
    if($scope.articuloSelected == 'Accesorios') {
      if(!$scope.complementoSelected ) return false;
    }

    if(!$scope.estadoSelected) return false;
    if(!$scope.temporadaSelected) return false;

    return true;
  }

  //onDragUp/Down
  $scope.hidebarflag = false;
  $scope.onDragUp = function() {
    $scope.dragup = true;
  }
  $scope.onDragDown = function() {
    $scope.dragdown = true;
  }
  $scope.release = function() {
    if($scope.dragup) {
      //ionic.Platform.fullScreen(false, false);
      //$ionicNavBarDelegate.showBar(false);
      if(document.getElementById("createproductbtn").className == 'botoCrearProducte showbutton') {
        document.getElementById("createproductbtn").className = 'botoCrearProducte hidebutton';
        document.getElementById("ordenar-bloque").className = 'ordenarBloque hidebuttonbar';
        document.getElementById("barstable").className = 'bar-stable hidebuttonbarJustMove';
      };
      $scope.dragup = false;

    }
    else if($scope.dragdown) {
      //ionic.Platform.fullScreen(false, true);
      //$ionicNavBarDelegate.showBar(true);
      //$ionicNavBarDelegate.title('Listado de productos');
      $scope.dragdown = false;
      if(document.getElementById("createproductbtn").className == 'botoCrearProducte hidebutton') {
        document.getElementById("createproductbtn").className = 'botoCrearProducte showbutton';
        document.getElementById("ordenar-bloque").className = 'ordenarBloque showbuttonbar';
        document.getElementById("barstable").className = 'bar-stable showbuttonbarJustMove';
      };
    }
  };

  //Open filters
  $scope.setFilters = function() {
    $ionicModal.fromTemplateUrl('templates/filter/filter.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
      $scope.modal.show();
    });
  }
  //Close create modal
  $scope.backToProducts = function() {
    $scope.modal.hide();
    $scope.modal.remove();
  }
  //Close filters
  $scope.isShowProduct = function(product) {
    //console.log('ola k ase', product);

    //FIRST CHECK IF THERE IS ANY FILTER APPLIED
    if($scope.sexofilter[0] == "active" || $scope.sexofilter[1] == "active" || $scope.sexofilter[2] == "active") {
      $scope.applysexfilter = true;
    }
    if($scope.articulofilter[0] == "active" || $scope.articulofilter[1] == "active" || $scope.articulofilter[2] == "active") {
      $scope.applyarticulofilter = true;
    }
    //IF ALL TRUE THEN NO APPLY FITLERING
    if(!$scope.applysexfilter && !$scope.applyarticulofilter) {
      //MAYBE THERE ARE NO FILTER, BUT WE WANT TO APPLY SLIDERS
      if(product.price < $scope.slidemin || product.price > $scope.slidemax) return false;

      //SLIDE EDAD MIX
      $scope.age2compare;
      if(product.genero=='Niño') $scope.age2compare = product.edad;
      else if(product.genero=='Niña') $scope.age2compare = product.edad;
      else if(product.genero=='Bebe') $scope.age2compare = product.mes;

      if($scope.slideminedadmix>8){
        console.log('$scope.slideminedadmix>8', $scope.age2compare, $scope.slideminedadmix-7, $scope.slidemaxedadmix-7);
        if($scope.slideminedadmix-7 > $scope.age2compare) return false;
      }
      else if($scope.slideminedadmix<=8){
        if($scope.slidemaxedadmix<=8){
          console.log('$scope.slideminedadmix<=8 && $scope.slidemaxedadmix<=8', $scope.age2compare, $scope.slideminedadmix*2, $scope.slidemaxedadmix*2);
          if(($scope.slidemaxedadmix*2 < $scope.age2compare) || ($scope.age2compare < $scope.slideminedadmix*2)) return false;
        }
        else {
          console.log('$scope.slideminedadmix<=8 && $scope.slidemaxedadmix>8', $scope.age2compare, $scope.slideminedadmix*2, $scope.slidemaxedadmix-7);
          if(($scope.slideminedadmix*2 > $scope.age2compare) || ($scope.age2compare > $scope.slidemaxedadmix-7)) return false;
        }
      }

      if(product.price < $scope.slideminedadbebe || product.price > $scope.slidemaxedadanos) return false;

      return true;
    }
    //SLIDE PRICE
    if(product.price < $scope.slidemin || product.price > $scope.slidemax) return false;

    if($scope.sexofilter[0] == "noactive" && product.genero == 'Niño') return false;
    if($scope.sexofilter[1] == "noactive" && product.genero == 'Niña') return false;
    if($scope.sexofilter[2] == "noactive" && product.genero == 'Bebe') return false;

    if($scope.articulofilter[0] == "noactive" && product.genero == 'Ropa') return false;
    if($scope.articulofilter[1] == "noactive" && product.genero == 'Calzado') return false;
    if($scope.articulofilter[2] == "noactive" && product.genero == 'Accesorios') return false;

    return true;
  }
  $scope.closeFilters = function() {
    // $scope.products = [];
    // $scope.applysexfilter = false;
    // $scope.applyarticulofilter = false;
    // if($scope.sexofilter[0] == "active" || $scope.sexofilter[1] == "active" || $scope.sexofilter[2] == "active") $scope.applysexfilter = true;
    // if($scope.articulofilter[0] == "active" || $scope.articulofilter[1] == "active" || $scope.articulofilter[2] == "active") $scope.applyarticulofilter = true;
    // $scope.allproducts.forEach(function(item, key){
    //   //console.log('pricemin,max: ', $scope.slidemin, $scope.slidemax);
    //
    //   if(item.price >= $scope.slidemin && item.price <= $scope.slidemax){
    //       if($scope.applysexfilter){
    //         if(item.genero == "Niño" && $scope.sexofilter[0] == "active") {
    //           if($scope.applyarticulofilter){
    //             if(item.articulo == "Ropa" && $scope.articulofilter[0] == "active") $scope.products.push(item);
    //             if(item.articulo == "Calzado" && $scope.articulofilter[1] == "active") $scope.products.push(item);
    //             if(item.articulo == "Accesorios" && $scope.articulofilter[2] == "active") $scope.products.push(item);
    //           }
    //           else $scope.products.push(item);
    //         }
    //         if(item.genero == "Niña" && $scope.sexofilter[1] == "active") {
    //           if($scope.applyarticulofilter){
    //             if(item.articulo == "Ropa" && $scope.articulofilter[0] == "active") $scope.products.push(item);
    //             if(item.articulo == "Calzado" && $scope.articulofilter[1] == "active") $scope.products.push(item);
    //             if(item.articulo == "Accesorios" && $scope.articulofilter[2] == "active") $scope.products.push(item);
    //           }
    //           else $scope.products.push(item);
    //         }
    //         if(item.genero == "Bebe" && $scope.sexofilter[2] == "active") {
    //           if($scope.applyarticulofilter){
    //             if(item.articulo == "Ropa" && $scope.articulofilter[0] == "active") $scope.products.push(item);
    //             if(item.articulo == "Calzado" && $scope.articulofilter[1] == "active") $scope.products.push(item);
    //             if(item.articulo == "Accesorios" && $scope.articulofilter[2] == "active") $scope.products.push(item);
    //           }
    //           else $scope.products.push(item);
    //         }
    //       }
    //       //NO SEX FILTER, YES ARTICULO FILTER
    //       if(!$scope.applysexfilter && $scope.applyarticulofilter){
    //         if(item.articulo == "Ropa" && $scope.articulofilter[0] == "active") $scope.products.push(item);
    //         if(item.articulo == "Calzado" && $scope.articulofilter[1] == "active") $scope.products.push(item);
    //         if(item.articulo == "Accesorios" && $scope.articulofilter[2] == "active") $scope.products.push(item);
    //       }
    //
    //       //NO FILTER TO APPLY
    //       if(!$scope.applysexfilter && !$scope.applyarticulofilter) $scope.products.push(item);
    //   }
    // })
    //
    // if($scope.filterorderby == 'El más nuevo') $scope.products.sort(function(a, b){return a.reftime-b.reftime});
    // else if($scope.filterorderby == 'price') $scope.products.sort(function(a, b){return a.price-b.price});

    $ionicScrollDelegate.scrollTop();
    $scope.modal.hide();
    $scope.modal.remove();
  };
  $scope.resetFilters = function() {
    $scope.selectedItems = [];
    $scope.sexofilter[0] = 'noactive';
    $scope.sexofilter[1] = 'noactive';
    $scope.sexofilter[2] = 'noactive';
    $scope.articulofilter[0] = 'noactive';
    $scope.articulofilter[1] = 'noactive';
    $scope.articulofilter[2] = 'noactive';

    $scope.slidemax = 1000;
    $scope.slidemin = 0;

    $scope.slidemaxedadbebe = 14;
    $scope.slideminedadbebe = 0;
    //$scope.eventHandlers.set([200, 800]);
    $scope.options = {
      animate: true,
      start: [$scope.slidemin, $scope.slidemax],
      margin: 20,
      tooltips: true,
      connect: true,
      step: 10,
      range: {min: 0, max: 1000}
    }
    $scope.optionsEdadBebe = {
      animate: true,
      start: [$scope.slideminedadbebe, $scope.slidemaxedadbebe],
      margin: 10,
      tooltips: true,
      connect: true,
      step: 1,
      range: {min: 0, max: 100}
    }
  }

  $scope.filterorderby = 'El más nuevo';
  $scope.selectFilterOrderBy = function(filterorderby) {
    $scope.filterorderby = filterorderby;
    console.log($scope.filterorderby);

    if($scope.filterorderby == 'El más nuevo') $scope.products.sort(function(a, b){return a.reftime-b.reftime});
    else if($scope.filterorderby == 'Por precio') $scope.products.sort(function(a, b){return a.price-b.price});
  }

  $scope.$on('$ionicView.enter', function(e) {
    console.log('ProductsCtrl');
  });

  var refphotos = new Firebase("https://kidsify.firebaseio.com/products");
  refphotos.once('value', function(snap) {
    //console.log(Object.keys(snap.val()).length);
    $scope.indexscroll = 20;
    $scope.indexscrolllimit = Object.keys(snap.val()).length;
  })
  var scrollRef = new Firebase.util.Scroll(refphotos,'reftime');
  //$scope.products = $firebaseArray(refphotos);
  $scope.products = $firebaseArray(scrollRef);
  scrollRef.scroll.next(20);
  $scope.moreDataCanBeLoaded = true;

  $scope.doRefresh = function() {
    $scope.$broadcast('scroll.refreshComplete');
  }


  $scope.superPoblate = function() {
    for(var i=0; i<1000; i++){
      //console.log($scope.allproducts[0]);
      $scope.allproducts.$add($scope.allproducts[0]);
    }

  }

  $scope.loadMore = function() {
    //console.log('loadMore');

    if($scope.indexscroll >= $scope.indexscrolllimit) {
      //console.log('FULL', $scope.indexscroll, $scope.indexscrolllimit);
      $scope.moreDataCanBeLoaded = false;
    }
    else {
      $scope.indexscroll += 20;
      scrollRef.scroll.next(20);
      $timeout(function() {
        $scope.$broadcast('scroll.infiniteScrollComplete');
      }, 1000);
    }
  }

  $scope.moreDataCanBeLoadedFunc = function(){
    return $scope.moreDataCanBeLoaded ;
  }

  /////////////////////////////////////////////////////////
  $scope.selectedItems = [];

  $scope.sexos = [
    {nom:'Niño',icon:'nino.svg'},
    {nom:'Niña',icon:'nina.svg'},
    {nom:'Bebé',icon:'bebe.svg'}
  ];

  $scope.ropas = [
    {nom:'Ropa',icon:'ropa.svg'},
    {nom:'Calzado',icon:'bambas.svg'},
    {nom:'Accesorios',icon:'accesorios.svg'}
    // {nom:'P. Corto',icon:'pantalon.svg'},
    // {nom:'P. Largo',icon:'pantalonlargo.svg'},
    // {nom:'Bambas',icon:'bambas.svg'},
    // {nom:'Zapatos',icon:'zapato.svg'}
  ];
  $scope.ropitas = [
    {nom:'Abrigo',icon:'ropa.svg'},
    {nom:'Camisa',icon:'camisa.svg'},
    {nom:'Camiseta',icon:'camiseta.svg'},
    {nom:'Falda',icon:'falda.svg'},
    {nom:'Jersey',icon:'jersey.svg'},
    {nom:'Mono',icon:'mono.svg'},
    {nom:'Pantalones',icon:'pantalones.svg'},
    {nom:'Polo',icon:'polo.svg'},
    {nom:'Vestido',icon:'vestido.svg'}
  ];
  $scope.calzados = [
    {nom:'Bambas',icon:'bambas.svg'},
    {nom:'Zapatos',icon:'zapatos.svg'},
    {nom:'Botas',icon:'botas.svg'},
    {nom:'Chanclas',icon:'chanclas.svg'}
  ];
  $scope.complementos = [
    {nom:'Bufanda',icon:'bufanda.svg'},
    {nom:'Gorro',icon:'gorro.svg'},
    {nom:'Calcetines',icon:'calcetines.svg'},
    {nom:'Guantes',icon:'guantes.svg'},
    {nom:'Cinturón',icon:'cinturon.svg'},
    {nom:'Tirantes',icon:'accesorios.svg'}
  ];
  $scope.edades = [
    {nom:'2 años',icon:'cake.svg'},
    {nom:'3 años',icon:'cake.svg'},
    {nom:'4 años',icon:'cake.svg'},
    {nom:'5 años',icon:'cake.svg'},
    {nom:'6 años',icon:'cake.svg'},
    {nom:'7 años',icon:'cake.svg'},
    {nom:'8 años',icon:'cake.svg'},
    {nom:'9 años',icon:'cake.svg'},
    {nom:'10 años',icon:'cake.svg'},
    {nom:'11 años',icon:'cake.svg'},
    {nom:'12 años',icon:'cake.svg'},
    {nom:'13 años',icon:'cake.svg'},
    {nom:'14 años',icon:'cake.svg'}
  ];

  $scope.meses = [
    {nom:'0 meses',icon:'cake.svg'},
    {nom:'1 mes',icon:'cake.svg'},
    {nom:'2 meses',icon:'cake.svg'},
    {nom:'3 meses',icon:'cake.svg'},
    {nom:'6 meses',icon:'cake.svg'},
    {nom:'9 meses',icon:'cake.svg'},
    {nom:'12 meses',icon:'cake.svg'},
    {nom:'18 meses',icon:'cake.svg'}
  ];

  $scope.temporadas = [
    {nom:'invierno',icon:'invierno.svg'},
    {nom:'Verano',icon:'verano.svg'},
    {nom:'Todo el año',icon:'primavera.svg'},
  ];

  $scope.estados = [
    {nom:'Bien',icon:'bueno.svg'},
    {nom:'Muy Bien',icon:'muybueno.svg'},
    {nom:'Nuevo',icon:'nuevo.svg'}
  ];

  $scope.slidemax = 1000;
  $scope.slidemin = 0;

  $scope.slidemaxedadbebe = 14;
  $scope.slideminedadbebe = 0;

  $scope.slidemaxedadmix = 21;
  $scope.slideminedadmix = 0;

  $scope.slidemaxedadanos = 14;
  $scope.slideminedadanos = 2;

  $scope.options = {
    animate: true,
    start: [$scope.slidemin, $scope.slidemax],
    margin: 100,
    connect: true,
    step: 10,
    tooltips: false,
    range: {min: 0, max: 1000}
  }
  $scope.optionsEdadBebe = {
    animate: true,
    start: [$scope.slideminedadbebe, $scope.slidemaxedadbebe],
    margin: 1,
    connect: true,
    step: 1,
    tooltips: false,
    range: {min: 0, max: 14}
  }
  $scope.optionsEdadMix = {
    animate: true,
    start: [$scope.slideminedadmix, $scope.slidemaxedadmix],
    margin: 1,
    connect: true,
    step: 1,
    tooltips: false,
    range: {min: 0, max: 21}
  }
  $scope.optionsEdadAnos = {
    animate: true,
    start: [$scope.slideminedadanos, $scope.slidemaxedadanos],
    margin: 1,
    connect: true,
    step: 1,
    tooltips: false,
    range: {min: 2, max: 14}
  }

  $scope.eventHandlers = {
    update: function(values, handle, unencoded) {
      //console.log('change', values[handle]);
      $scope.slidemax = parseInt(values[0][1]);
      $scope.slidemin = parseInt(values[0][0]);
    }
  }
  $scope.eventHandlersEdadBebe = {
    update: function(values, handle, unencoded) {
      //console.log('change', values[handle]);
      $scope.slidemaxedadbebe = parseInt(values[0][1]);
      $scope.slideminedadbebe = parseInt(values[0][0]);
    }
  }
  $scope.eventHandlersEdadMix = {
    update: function(values, handle, unencoded) {
      //console.log('change', values[handle]);
      $scope.slidemaxedadmix = parseInt(values[0][1]);
      $scope.slideminedadmix = parseInt(values[0][0]);
    }
  }
  $scope.eventHandlersEdadAnos = {
    update: function(values, handle, unencoded) {
      //console.log('change', values[handle]);
      $scope.slidemaxedadanos = parseInt(values[0][1]);
      $scope.slideminedadanos = parseInt(values[0][0]);
    }
  }

  $scope.isItemSelected = function(num){
    var count=0;
    $scope.selectedItems.forEach(function(item){
      if(item==num)count++;
    });
    //console.log(count>0);

    return count>0;
  }
  $scope.select = function(num){
    //console.log(num, $scope.generoSelected, $scope.articuloSelected);

    var count=0;
    var position=0;
    var lastposition=0;
    var positionprev=0;
    var positionnext=0;
    if(num==700) {

      positionprev = 701;
      positionnext = 702;
      //console.log(positionprev, positionnext);
    }
    else if(num==701) {
      positionprev = 700;
      positionnext = 702;
      //console.log(positionprev, positionnext);
    }
    else if(num==702) {
      positionprev = 700;
      positionnext = 701;
      //console.log(positionprev, positionnext);
    }
    $scope.selectedItems.forEach(function(item,key){
      if(item==num){
        count++;
        position = key;
        //console.log(key);
      }
    });
    if(count>0){
      $scope.selectedItems.splice(position,1);

      if(num == 0) $scope.sexofilter[0] = 'noactive';
      else if(num == 1) $scope.sexofilter[1] = 'noactive';
      else if(num == 2) $scope.sexofilter[2] = 'noactive';

      if(num == 300) $scope.articulofilter[0] = 'noactive';
      else if(num == 301) $scope.articulofilter[1] = 'noactive';
      else if(num == 302) $scope.articulofilter[2] = 'noactive';

    }
    else {
      //console.log('else && num: ', num);
      $scope.selectedItems.push(num);
      if(num==700 || num==701 || num==702){
        //console.log('splice ', positionprev, positionnext);
        $scope.selectedItems.splice(positionprev,1);
        $scope.selectedItems.splice(positionnext,1);
      }

      if(num == 0) $scope.sexofilter[0] = 'active';
      else if(num == 1) $scope.sexofilter[1] = 'active';
      else if(num == 2) $scope.sexofilter[2] = 'active';

      if(num == 300) {
        $scope.articulofilter[0] = 'active';
        //console.log(num, $scope.articulofilter[0]);
      }
      else if(num == 301) {
        $scope.articulofilter[1] = 'active';
        //console.log($scope.articulofilter[1]);
      }
      else if(num == 302) {
        $scope.articulofilter[2] = 'active';
        //console.log($scope.articulofilter[2]);
      }
    }


  }

  $scope.estatus = "desactivat";

  $scope.sexofilter = ['noactive','noactive','noactive'];
  $scope.articulofilter = ['noactive','noactive','noactive'];

  $scope.activaCarac = function(num) {
    $scope.select(num);
    console.log("entro: ", num);



    if ($scope.estatus === "desactivat")
        $scope.estatus = "activat";
    else
        $scope.estatus = "desactivat";

  };

  $scope.swiperOptions = {

    /* Whatever options */
    effect: 'slide',
    paginationClickable: true,
    slidesPerView: 3,
    spaceBetween: 10,
    preloadImages: true,
    updateOnImagesReady: true,
    speed: 100,
    direction: 'horizontal',
    /* Initialize a scope variable with the swiper */
    onInit: function(swiper){
      $scope.swiper = swiper;
    }
  };

  $scope.isItemSelectedProduct = function(num){
    if(num == 0 && $scope.generoSelected=='Niño') return true;
    else if(num == 1 && $scope.generoSelected=='Niña') return true;
    else if(num == 2 && $scope.generoSelected=='Bebe') return true;

    if(num == 100 && $scope.edadSelected==2) return true;
    else if(num == 101 && $scope.edadSelected==3) return true;
    else if(num == 102 && $scope.edadSelected==4) return true;
    else if(num == 103 && $scope.edadSelected==5) return true;
    else if(num == 104 && $scope.edadSelected==6) return true;
    else if(num == 105 && $scope.edadSelected==7) return true;
    else if(num == 106 && $scope.edadSelected==8) return true;
    else if(num == 107 && $scope.edadSelected==9) return true;
    else if(num == 108 && $scope.edadSelected==10) return true;
    else if(num == 109 && $scope.edadSelected==11) return true;
    else if(num == 110 && $scope.edadSelected==12) return true;
    else if(num == 111 && $scope.edadSelected==13) return true;
    else if(num == 112 && $scope.edadSelected==14) return true;

    if(num == 200 && $scope.mesSelected==0) return true;
    else if(num == 201 && $scope.mesSelected==1) return true;
    else if(num == 202 && $scope.mesSelected==2) return true;
    else if(num == 203 && $scope.mesSelected==3) return true;
    else if(num == 204 && $scope.mesSelected==6) return true;
    else if(num == 205 && $scope.mesSelected==9) return true;
    else if(num == 206 && $scope.mesSelected==12) return true;
    else if(num == 207 && $scope.mesSelected==18) return true;

    if(num == 300 && $scope.articuloSelected=='Ropa') return true;
    else if(num == 301 && $scope.articuloSelected=='Calzado') return true;
    else if(num == 302 && $scope.articuloSelected=='Accesorios') return true;

    if(num == 400 && $scope.ropaSelected=='Abrigo') return true;
    else if(num == 401 && $scope.ropaSelected=='Camisa') return true;
    else if(num == 402 && $scope.ropaSelected=='Camiseta') return true;
    else if(num == 403 && $scope.ropaSelected=='Falda') return true;
    else if(num == 404 && $scope.ropaSelected=='Jersey') return true;
    else if(num == 405 && $scope.ropaSelected=='Mono') return true;
    else if(num == 406 && $scope.ropaSelected=='Pantalones') return true;
    else if(num == 407 && $scope.ropaSelected=='Polo') return true;
    else if(num == 408 && $scope.ropaSelected=='Vestido') return true;

    if(num == 500 && $scope.calzadoSelected=='Bambas') return true;
    else if(num == 501 && $scope.calzadoSelected=='Zapatos') return true;
    else if(num == 502 && $scope.calzadoSelected=='Botas') return true;
    else if(num == 503 && $scope.calzadoSelected=='Chanclas') return true;

    if(num == 600 && $scope.complementoSelected=='Bufanda') return true;
    else if(num == 601 && $scope.complementoSelected=='Gorro') return true;
    else if(num == 602 && $scope.complementoSelected=='Calcetines') return true;
    else if(num == 603 && $scope.complementoSelected=='Guantes') return true;
    else if(num == 604 && $scope.complementoSelected=='Cinturón') return true;
    else if(num == 605 && $scope.complementoSelected=='Tirantes') return true;

    if(num == 700 && $scope.temporadaSelected=='Otoño/Invierno') return true;
    else if(num == 701 && $scope.temporadaSelected=='Primavera/Verano') return true;
    else if(num == 702 && $scope.temporadaSelected=='Todo el año') return true;

    if(num == 800 && $scope.estadoSelected=='Bien') return true;
    else if(num == 801 && $scope.estadoSelected=='Muy Bien') return true;
    else if(num == 802 && $scope.estadoSelected=='Nuevo') return true;
  }
  $scope.activaCaracProduct = function(num) {
    $scope.selectProduct(num);
    //console.log("entro: ", num);
  };
  $scope.selectProduct = function(num){
    if(num == 0) $scope.generoSelected='Niño';
    else if(num == 1) $scope.generoSelected='Niña';
    else if(num == 2) $scope.generoSelected='Bebe';

    if(num == 100) $scope.edadSelected=2;
    else if(num == 101) $scope.edadSelected=3;
    else if(num == 102) $scope.edadSelected=4;
    else if(num == 103) $scope.edadSelected=5;
    else if(num == 104) $scope.edadSelected=6;
    else if(num == 105) $scope.edadSelected=7;
    else if(num == 106) $scope.edadSelected=8;
    else if(num == 107) $scope.edadSelected=9;
    else if(num == 108) $scope.edadSelected=10;
    else if(num == 109) $scope.edadSelected=11;
    else if(num == 110) $scope.edadSelected=12;
    else if(num == 111) $scope.edadSelected=13;
    else if(num == 112) $scope.edadSelected=14;

    if(num == 200) $scope.mesSelected=0;
    else if(num == 201) $scope.mesSelected=1;
    else if(num == 202) $scope.mesSelected=2;
    else if(num == 203) $scope.mesSelected=3;
    else if(num == 204) $scope.mesSelected=6;
    else if(num == 205) $scope.mesSelected=9;
    else if(num == 206) $scope.mesSelected=12;
    else if(num == 207) $scope.mesSelected=18;

    if(num == 300) $scope.articuloSelected='Ropa';
    else if(num == 301) $scope.articuloSelected='Calzado';
    else if(num == 302) $scope.articuloSelected='Accesorios';

    if(num == 400) $scope.ropaSelected='Abrigo';
    else if(num == 401) $scope.ropaSelected='Camisa';
    else if(num == 402) $scope.ropaSelected='Camiseta';
    else if(num == 403) $scope.ropaSelected='Falda';
    else if(num == 404) $scope.ropaSelected='Jersey';
    else if(num == 405) $scope.ropaSelected='Mono';
    else if(num == 406) $scope.ropaSelected='Pantalones';
    else if(num == 407) $scope.ropaSelected='Polo';
    else if(num == 408) $scope.ropaSelected='Vestido';

    if(num == 500) $scope.calzadoSelected='Bambas';
    else if(num == 501) $scope.calzadoSelected='Zapatos';
    else if(num == 502) $scope.calzadoSelected='Botas';
    else if(num == 503) $scope.calzadoSelected='Chanclas';

    if(num == 600) $scope.complementoSelected='Bufanda';
    else if(num == 601) $scope.complementoSelected='Gorro';
    else if(num == 602) $scope.complementoSelected='Calcetines';
    else if(num == 603) $scope.complementoSelected='Guantes';
    else if(num == 604) $scope.complementoSelected='Cinturón';
    else if(num == 605) $scope.complementoSelected='Tirantes';

    if(num == 700) $scope.temporadaSelected='Otoño/Invierno';
    else if(num == 701) $scope.temporadaSelected='Primavera/Verano';
    else if(num == 702) $scope.temporadaSelected='Todo el año';

    if(num == 800) $scope.estadoSelected='Bien';
    else if(num == 801) $scope.estadoSelected='Muy Bien';
    else if(num == 802) $scope.estadoSelected='Nuevo';
  }

  $scope.goToProductDetail = function(product) {
    ProductDetail.setProduct(product);

    $state.go('productdetail');
  }
  ////////////////////////////////////////////////////////

  //COPY OF CREATE^RPDUCT CONTROLLER
  //button callback
  $scope.addMedia = function() {

    $scope.hideSheet = $ionicActionSheet.show({
        buttons: [
          {text: 'Hacer foto'},
          {text: 'Coger foto del teléfono'},
          {text: 'Hacer vídeo'}
        ],
        titleText: 'Añadir Imagen o vídeo',
        cancelText: 'Cancelar',
        buttonClicked: function(index){
          $scope.addImage(index);
        }
    })
  };

  //Check option to load or take a picture & callback
  $scope.addImage = function(type) {
    $scope.hideSheet();
    if(type==0) $scope.upload();
    else if(type==1) $scope.getPicture();
    else if(type==2) $scope.capture();
  };

  //take picture (need ngCordova camera plugin)
  $scope.nomorephotos = false;
  $scope.deletePhoto = function(index) {
    $scope.images.splice(index,1);
    if($scope.images.length == 4) $scope.nomorephotos = true;
    else $scope.nomorephotos = false;
  }

  var importFile = function (file) {
    var reader = new FileReader();
    alert('dentro');
    reader.onloadend = function (e) {
        // do your parsing here
        alert($filter('json')(e));
        alert($filter('json')(e.target));
        alert($filter('json')(e.target.result));
    };
    alert($filter('json')(file));
    //reader.readAsText(file);
    reader.readAsDataURL(file);
  }

  $scope.videoData = {};
  var videobase64 = {};
  $scope.capture = function() {
    var options = { limit: 3, duration: 10};

    $cordovaCapture.captureVideo(options).then(function(videoData) {
        $scope.videoData = videoData;

        window.resolveLocalFileSystemURL(videoData[0].localURL,
            function (entry) {
                entry.file(
                    function (file) {
                      //alert('reading file');
                      var reader = new FileReader();
                      reader.onloadend = function (e) {
                          // do your parsing here
                          videobase64 = e.target.result;
                      };

                      reader.readAsDataURL(file);
                    },
                    function (error) {
                        //console.log("FileEntry.file error: " + error.code);
                    }
                );
            },
            function (error) {
                //console.log("resolveLocalFileSystemURL error: " + error.code);
            });

    }, function(err) {
      // An error occurred. Show a message to the user
      //alert('error taking video', err)
    });
  }

  $scope.getPicture = function() {
    var options = {
        quality : 75,
        destinationType : Camera.DestinationType.DATA_URL,
        sourceType : Camera.PictureSourceType.PHOTOLIBRARY,
        allowEdit : true,
        encodingType: Camera.EncodingType.JPEG,
        popoverOptions: CameraPopoverOptions,
        targetWidth: 500,
        targetHeight: 500,
        saveToPhotoAlbum: false
    };
    $cordovaCamera.getPicture(options).then(function(imageData) {
        console.log(imageData);
        $scope.images.push(imageData);
        //uploadLogo(imageData);
        if($scope.images.length == 3) $scope.nomorephotos = true;
        else $scope.nomorephotos = false;
    }, function(error) {
        console.error(error);
    });
  };

  //getPicture from library
  $scope.upload = function() {
     var options = {
         quality : 75,
         destinationType : Camera.DestinationType.DATA_URL,
         sourceType : Camera.PictureSourceType.CAMERA,
         allowEdit : true,
         encodingType: Camera.EncodingType.JPEG,
         popoverOptions: CameraPopoverOptions,
         targetWidth: 500,
         targetHeight: 500,
         saveToPhotoAlbum: false
     };
     $cordovaCamera.getPicture(options).then(function(imageData) {
       console.log(imageData);
       $scope.images.push(imageData);
       if($scope.images.length == 3) $scope.nomorephotos = true;
       else $scope.nomorephotos = false;
     }, function(error) {
         console.error(error);
     });
  }

  $scope.amazonPush = function(images){
    //console.log(images);
    //uploadLogo(images);
  }

  $scope.selectGenero = function(val){
    $scope.generoSelected = val;
  }
  $scope.selectArticulo = function(val){
    $scope.articuloSelected = val;
  }

  $scope.selectEdad = function(val){
    $scope.edadSelected = val;
  }

  $scope.selectMes = function(val){
    $scope.selectMes = val;
  }

  //////AMAZON
  $scope.sizeLimit      = 10585760; // 10MB in Bytes
  $scope.uploadProgress = 0;
  $scope.creds          = {};

  $scope.creds.access_key="AKIAIQTXU2EEQWS37R3Q";
  $scope.creds.secret_key="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";
  $scope.creds.bucket="kidsify";

  var refuser = new Firebase("https://kidsify.firebaseio.com/users").child(window.localStorage.getItem('uid'));
  $scope.user.numproducts = $firebaseObject(refuser);
  var uploadLogo = function(images, keyref, ref) {
    $scope.user.numproducts++;

    var refuserproduct = new Firebase("https://kidsify.firebaseio.com/userproducts").child(window.localStorage.getItem('uid')).child('products');
    AWS.config.update({ accessKeyId: $scope.creds.access_key, secretAccessKey: $scope.creds.secret_key });
    AWS.config.region = 'us-east-1';
    var bucket = new AWS.S3({ params: { Bucket: $scope.creds.bucket } });
    //console.log(images);
    var array = [];
    angular.forEach(images,function(image, key){
      //console.log(key, image);

      if(image) {
          // Perform File Size Check First
          var fileSize = Math.round(parseInt(image.size));
          if (fileSize > $scope.sizeLimit) {
            console.error('Sorry, your attachment is too big. <br/> Maximum '  + $scope.fileSizeLabel() + ' file attachment allowed','File Too Large');
            return false;
          }
          // Prepend Unique String To Prevent Overwrites
          var uniqueFileName = "logo"+$scope.uniqueString() + '-hjk' + key;
          //alert(uniqueString);
          $scope.urlProducts = "https://kidsify.s3.amazonaws.com/products/"+keyref+"/"+uniqueFileName;
          var params = { Key: "products/"+keyref+"/"+uniqueFileName, ContentType: image.type, Body: convertDataURIToBinary('data:image/jpeg;base64,'+image), ServerSideEncryption: 'AES256' };
          //alert("image type,", image.type);

          array.push($scope.urlProducts);
          // alert(array);

          bucket.putObject(params, function(err, data) {
            if(err) {
              console.error(err.message,err.code);
              return false;
            }
            else {
              setTimeout(function() {
                $scope.uploadProgress = 0;
                $scope.$digest();

              }, 2500);

              setTimeout(function() {
                 // $state.go($state.current, {}, {reload: true});
                }, 1000);
                if($scope.uploadProgress==100) {
                  //alert('Push 2 Firebase');
                  $cordovaGeolocation
        					.getCurrentPosition({timeout: 10000, enableHighAccuracy: false})
        					.then(function (position) {
                    //alert('Got position update!');

                    if(!$scope.edadSelected) $scope.edadSelected='';
                    if(!$scope.mesSelected) $scope.mesSelected='';
                    if(!$scope.ropaSelected) $scope.ropaSelected='';
                    if(!$scope.calzadoSelected) $scope.calzadoSelected='';
                    if(!$scope.complementoSelected) $scope.complementoSelected='';

                    ref.set({
                        owner: window.localStorage.getItem('uid'),
                        title: $scope.product.name,
                        description: $scope.product.description,
                        price: $scope.product.price,
                        image: array,
                        genero: $scope.generoSelected,
                        edad: $scope.edadSelected,
                        mes: $scope.mesSelected,
                        articulo: $scope.articuloSelected,
                        ropa: $scope.ropaSelected,
                        calzado: $scope.calzadoSelected,
                        complemeto: $scope.complementoSelected,
                        temporada: $scope.temporadaSelected,
                        estado: $scope.estadoSelected,
                        lat: position.coords.latitude,
                        lng: position.coords.longitude,
                        reftime: -(new Date().getTime())
                    })
                    refuser.update({
                        numproducts: $scope.user.numproducts
                      })
                    refuserproduct.push({
                          assigneduser: '',
                          state: 'pending',
                          productkey: keyref
                    })
                    alert('imageUpdated');
                  })
                }
            }
          })
          .on('httpUploadProgress',function(progress) {
            $scope.uploadProgress = Math.round(progress.loaded / progress.total * 100);
            $scope.$digest();

          });
        }
        else {
          // No File Selected
          //toastr.error('Please select a file to upload');
          console.error('Please select a file to upload');
        }
      });
    }
    var uploadVideo = function(video, keyref, ref) {

      var refuserproduct = new Firebase("https://kidsify.firebaseio.com/userproducts").child(window.localStorage.getItem('uid')).child('products');


      AWS.config.update({ accessKeyId: $scope.creds.access_key, secretAccessKey: $scope.creds.secret_key });
      AWS.config.region = 'us-east-1';
      var bucket = new AWS.S3({ params: { Bucket: $scope.creds.bucket } });
      //console.log(images);
      var array = [];

        //console.log(key, image);
        if(video) {

            // Prepend Unique String To Prevent Overwrites
            var uniqueFileName = "logo"+$scope.uniqueString() + '-hjk82';

            //alert(uniqueString);
            $scope.urlProducts = "https://kidsify.s3.amazonaws.com/products/video/"+keyref+"/"+uniqueFileName;
            alert(video.type);
            var params = { Key: "products/video/"+keyref+"/"+uniqueFileName, ContentType: "video/mp4", Body: convertDataURIToBinary(videobase64), ServerSideEncryption: 'AES256' };

            array.push($scope.urlProducts);
            // alert(array);

            bucket.putObject(params, function(err, data) {
              if(err) {
                console.error(err.message,err.code);
                return false;
              }
              else {
                setTimeout(function() {
                  $scope.uploadProgress = 0;
                  $scope.$digest();

                }, 2500);
                if($scope.uploadProgress==100) {
                    ref.update({
                        video: array
                    })
                }

                alert('Success');
              }
            })
            .on('httpUploadProgress',function(progress) {
              $scope.uploadProgress = Math.round(progress.loaded / progress.total * 100);
              $scope.$digest();

            });
          }
          else {
            // No File Selected
            //toastr.error('Please select a file to upload');
            console.error('Please select a file to upload');
          }

      }

    var BASE64_MARKER = ';base64,';
    function convertDataURIToBinary(dataURI) {
      var base64Index = dataURI.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
      var base64 = dataURI.substring(base64Index);
      var raw = window.atob(base64);
      var rawLength = raw.length;
      var array = new Uint8Array(new ArrayBuffer(rawLength));

      for(i = 0; i < rawLength; i++) {
        array[i] = raw.charCodeAt(i);
      }
      return array;
    }

    $scope.uniqueString = function() {
      var text     = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

      for( var i=0; i < 8; i++ ) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
      }
      return text;
    }

    $scope.fileSizeLabel = function() {
    // Convert Bytes To MB
    return Math.round($scope.sizeLimit / 1024 / 1024) + 'MB';
  };
  ////////

  //Post product
  $scope.postProduct = function() {
    //console.log('postProduct');
    var refphotos = new Firebase("https://kidsify.firebaseio.com/products");
    var newrefis = refphotos.push();

    uploadLogo($scope.images, newrefis.key(), newrefis);
    setTimeout(function(){
      uploadVideo($scope.videoData[0], newrefis.key(), newrefis);
    }, 1000);

    $scope.backToProducts();
  }


})

app.controller('LoginCtrl', function($scope, $state, $firebaseArray, $ionicPlatform, $firebaseAuth) {

  var ref = new Firebase("https://kidsify.firebaseio.com/users");

  $scope.data = {};

  ///// REGISTER /////////
  $scope.registerEmail = function(){

    ref.createUser({
      email    : $scope.data.email,
      password : $scope.data.password
    }, function(error, userData) {
      if (error) {
        //console.log("Error creating user:", error);
        alert("Error", error);
      } else {
        var refe = new Firebase("https://kidsify.firebaseio.com/users");
        //console.log(userData.provider, userData.uid, $scope.data.username, $scope.data.email, $scope.data.phonenumber);
        refe.child(userData.uid).update({
          provider: '',
          uid: userData.uid,
          name: $scope.data.name,
          image: '',
          email: $scope.data.email,
          phonenumber: '',
          numproducts: 0,
          numfollowers: 0,
          numfollowing: 0
        });

        //PUSH NOTIFICATIONS CONFIG WITH LOCALSTORAGE
        window.localStorage.setItem("uid", userData.uid);
        document.addEventListener('deviceready', function () {
            window.plugins.OneSignal.getIds(function(ids) {
              window.localStorage.setItem("pushUid", ids.userId);
              refe.child(userData.uid).child('pushUid').push({'id': ids.userId});
            });
        });

        //GO TO NEXT STATE
        $state.go('welcome');
      }
    });
  };

///// LOGIN FACEBOOK //////

$scope.login = function(){
     if(!$scope.isLoggedIn()){
          //------
        $ionicPlatform.ready(function() {
          facebookConnectPlugin.login(['public_profile','email'], function(status) {
            facebookConnectPlugin.getAccessToken(function(token) {
              // Authenticate with Facebook using an existing OAuth 2.0 access token
              ref.authWithOAuthToken("facebook", token, function(error, authData) {
                if (error) {
                  alert('Firebase login failed!', error);
                } else {
                  alert('PUSH FIREBASE');
                    //FIREBASE PUSH USER
                    var refe = new Firebase("https://kidsify.firebaseio.com/users");
                    refe.child(authData.uid).update({
                      provider: authData.provider,
                      uid: authData.uid,
                      name: authData.facebook.displayName,
                      image: authData.facebook.profileImageURL,
                      email: authData.facebook.email,
                      phonenumber: '',
                      numproducts: 0,
                      numfollowers: 0,
                      numfollowing: 0
                    });

                  alert('Authenticated successfully with payload:', authData);
                  window.localStorage.setItem("uid", authData.uid);
                  $state.go('app.products');
                }
              });
            }, function(error) {
              alert('Could not get access token', error);
            });
          }, function(error) {
            alert('An error occurred logging the user in', error);
          });
          //------
        });
      }
      else{
        alert("Welcome back " + window.localStorage.getItem("uid"));
        $state.go('app.products');
      }
  };

///// LOGIN /////////
$scope.loginEmail = function() {
  if(!$scope.isLoggedIn()){
        ref.authWithPassword({
          email    : $scope.data.email,
          password : $scope.data.password
        }, function(error, authData) {
          if (error) {
            alert(error);
            console.log("Login Failed!", error);
          } else {
            //FIREBASE PUSH USER
            ref.child(authData.uid).update({
              provider: authData.provider
             });

            //PUSH NOTIFICATIONS CONFIG WITH
            window.localStorage.setItem("uid", authData.uid);
            document.addEventListener('deviceready', function () {
              window.plugins.OneSignal.getIds(function(ids) {
                window.localStorage.setItem("pushUid", ids.userId);
                ref.child(authData.uid).child('pushUid').push({'id': ids.userId});
              });
            });

            //LOCALSTORAGE IDENTIFY YOUR USER -- GO TO NEXT STATE
            $state.go('app.products');
          }
        });
    }
    else{
      //LOCALSTORAGE IDENTIFY YOUR USER -- GO TO NEXT STATE
      $state.go('app.products');
    }
};

//FUNCTION TO CHECK IF YOUR USER IS LOGGED
$scope.isLoggedIn = function() {
  if(window.localStorage.getItem("uid") != undefined) return true;
  return false;
}



})

app.controller('CreateProductCtrl', function($scope, $state, $firebaseObject, $firebaseArray, $filter, $cordovaCapture, $timeout, $cordovaGeolocation, $ionicActionSheet, $ionicModal, $cordovaCamera) {
  //button callback
  $scope.addMedia = function() {

    $scope.hideSheet = $ionicActionSheet.show({
        buttons: [
          {text: 'Hacer foto'},
          {text: 'Coger foto del teléfono'},
          {text: 'Hacer vídeo'}
        ],
        titleText: 'Añadir imagen o vídeo',
        cancelText: 'Cancelar',
        buttonClicked: function(index){
          $scope.addImage(index);
        }
    })
  };

  $scope.sexos = [
    {nom:'Niño',icon:'nino.svg'},
    {nom:'Niña',icon:'nina.svg'},
    {nom:'Bebé',icon:'bebe.svg'}
  ];

  $scope.ropas = [
    {nom:'Ropa',icon:'ropa.svg'},
    {nom:'Calzado',icon:'bambas.svg'},
    {nom:'Accesorios',icon:'accesorios.svg'}
    // {nom:'P. Corto',icon:'pantalon.svg'},
    // {nom:'P. Largo',icon:'pantalonlargo.svg'},
    // {nom:'Bambas',icon:'bambas.svg'},
    // {nom:'Zapatos',icon:'zapato.svg'}
  ];

  $scope.edades = [
    {nom:'2 años',icon:'cake.svg'},
    {nom:'3 años',icon:'cake.svg'},
    {nom:'4 años',icon:'cake.svg'},
    {nom:'5 años',icon:'cake.svg'},
    {nom:'6 años',icon:'cake.svg'},
    {nom:'7 años',icon:'cake.svg'},
    {nom:'8 años',icon:'cake.svg'},
    {nom:'9 años',icon:'cake.svg'}
  ];

  $scope.meses = [
    {nom:'0 meses',icon:'cake.svg'},
    {nom:'1 mes',icon:'cake.svg'},
    {nom:'2 meses',icon:'cake.svg'},
    {nom:'3 meses',icon:'cake.svg'},
    {nom:'6 meses',icon:'cake.svg'},
    {nom:'9 meses',icon:'cake.svg'},
    {nom:'12 meses',icon:'cake.svg'},
    {nom:'18 meses',icon:'cake.svg'}
  ];

  $scope.temporadas = [
    {nom:'Otoño/Invierno',icon:'invierno.svg'},
    {nom:'Primavera/Verano',icon:'primavera.svg'},
    {nom:'Todo el año',icon:'verano.svg'}
  ];

  $scope.estados = [
    {nom:'Bien',icon:'bueno.svg'},
    {nom:'Muy Bien',icon:'muybueno.svg'},
    {nom:'Nuevo',icon:'nuevo.svg'}
  ];
  //Check option to load or take a picture & callback
  $scope.addImage = function(type) {
    $scope.hideSheet();
    if(type==0) $scope.upload();
    else if(type==1) $scope.getPicture();
    else if(type==2) $scope.capture();
  };

  $scope.canUpdate = function() {
    //if($scope.images.length==0) return false;
    // if($scope.videoData.length==0) return false;
    if(!$scope.product.name) return false;
    if(!$scope.product.description) return false;
    if(!$scope.product.price) return false;

    if(!$scope.generoSelected) return false;
    if($scope.generoSelected == 'Niño' || $scope.generoSelected == 'Niña'){
      if(!$scope.edadSelected) return false;
    }
    if($scope.generoSelected == 'Bebe'){
      if(!$scope.mesSelected) return false;
    }

    if(!$scope.articuloSelected) return false;
    if($scope.articuloSelected == 'Ropa') {
      if(!$scope.ropaSelected ) return false;
    }
    if($scope.articuloSelected == 'Calzado') {
      if(!$scope.calzadoSelected ) return false;
    }
    if($scope.articuloSelected == 'Accesorios') {
      if(!$scope.complementoSelected ) return false;
    }

    if(!$scope.estadoSelected) return false;
    if(!$scope.temporadaSelected) return false;

    return true;
  }

  //take picture (need ngCordova camera plugin)
  $scope.nomorephotos = false;
  $scope.deletePhoto = function(index) {
    $scope.images.splice(index,1);
    if($scope.images.length == 4) $scope.nomorephotos = true;
    else $scope.nomorephotos = false;
  }
  $scope.videoData = {};
  var videobase64 = {};
  $scope.capture = function() {
    var options = { limit: 3, duration: 10 };

    $cordovaCapture.captureVideo(options).then(function(videoData) {
        $scope.videoData = videoData;

        window.resolveLocalFileSystemURL(videoData[0].localURL,
            function (entry) {
              //alert('entry');
                entry.file(

                    function (file) {
                      //alert('reading file');
                      var reader = new FileReader();
                      reader.onloadend = function (e) {
                          // do your parsing here
                          //alert(e.target.result);
                          videobase64 = e.target.result;

                      };
                      alert($filter('json')(file));
                      reader.readAsDataURL(file);
                    },
                    function (error) {
                      //alert('fuckerr')
                        //console.log("FileEntry.file error: " + error.code);
                    }
                );
            },
            function (error) {
                //alert('fuck1eer2');
                //console.log("resolveLocalFileSystemURL error: " + error.code);
            });

    }, function(err) {
      // An error occurred. Show a message to the user
    });
  }
  $scope.getPicture = function() {
    var options = {
        quality : 75,
        destinationType : Camera.DestinationType.DATA_URL,
        sourceType : Camera.PictureSourceType.PHOTOLIBRARY,
        allowEdit : true,
        encodingType: Camera.EncodingType.JPEG,
        popoverOptions: CameraPopoverOptions,
        targetWidth: 500,
        targetHeight: 500,
        saveToPhotoAlbum: false
    };
    $cordovaCamera.getPicture(options).then(function(imageData) {
        console.log(imageData);
        $scope.images.push(imageData);
        if($scope.images.length == 3) $scope.nomorephotos = true;
        else $scope.nomorephotos = false;
        //uploadLogo(imageData);
    }, function(error) {
        console.error(error);
    });
  };

  //getPicture from library
  $scope.upload = function() {
     var options = {
         quality : 75,
         destinationType : Camera.DestinationType.DATA_URL,
         sourceType : Camera.PictureSourceType.CAMERA,
         allowEdit : true,
         encodingType: Camera.EncodingType.JPEG,
         popoverOptions: CameraPopoverOptions,
         targetWidth: 500,
         targetHeight: 500,
         saveToPhotoAlbum: false
     };
     $cordovaCamera.getPicture(options).then(function(imageData) {
       console.log(imageData);
       $scope.images.push(imageData);
       if($scope.images.length == 3) $scope.nomorephotos = true;
       else $scope.nomorephotos = false;
     }, function(error) {
         console.error(error);
     });
  }

  $scope.amazonPush = function(images){
    //console.log(images);
    //uploadLogo(images);
  }

  $scope.selectGenero = function(val){
    $scope.generoSelected = val;
  }
  $scope.selectArticulo = function(val){
    $scope.articuloSelected = val;
  }
  $scope.selectEdad = function(val){
    $scope.edadSelected = val;
  }
  $scope.selectMes = function(val){
    $scope.mesSelected = val;
  }
  $scope.isItemSelectedProduct = function(num){
    if(num == 0 && $scope.generoSelected=='Niño') return true;
    else if(num == 1 && $scope.generoSelected=='Niña') return true;
    else if(num == 2 && $scope.generoSelected=='Bebe') return true;

    if(num == 100 && $scope.edadSelected==2) return true;
    else if(num == 101 && $scope.edadSelected==3) return true;
    else if(num == 102 && $scope.edadSelected==4) return true;
    else if(num == 103 && $scope.edadSelected==5) return true;
    else if(num == 104 && $scope.edadSelected==6) return true;
    else if(num == 105 && $scope.edadSelected==7) return true;
    else if(num == 106 && $scope.edadSelected==8) return true;
    else if(num == 107 && $scope.edadSelected==9) return true;
    else if(num == 108 && $scope.edadSelected==10) return true;
    else if(num == 109 && $scope.edadSelected==11) return true;
    else if(num == 110 && $scope.edadSelected==12) return true;
    else if(num == 111 && $scope.edadSelected==13) return true;
    else if(num == 112 && $scope.edadSelected==14) return true;

    if(num == 200 && $scope.mesSelected==0) return true;
    else if(num == 201 && $scope.mesSelected==1) return true;
    else if(num == 202 && $scope.mesSelected==2) return true;
    else if(num == 203 && $scope.mesSelected==3) return true;
    else if(num == 204 && $scope.mesSelected==6) return true;
    else if(num == 205 && $scope.mesSelected==9) return true;
    else if(num == 206 && $scope.mesSelected==12) return true;
    else if(num == 207 && $scope.mesSelected==18) return true;

    if(num == 300 && $scope.articuloSelected=='Ropa') return true;
    else if(num == 301 && $scope.articuloSelected=='Calzado') return true;
    else if(num == 302 && $scope.articuloSelected=='Accesorios') return true;

    if(num == 400 && $scope.ropaSelected=='Abrigo') return true;
    else if(num == 401 && $scope.ropaSelected=='Camisa') return true;
    else if(num == 402 && $scope.ropaSelected=='Camiseta') return true;
    else if(num == 403 && $scope.ropaSelected=='Falda') return true;
    else if(num == 404 && $scope.ropaSelected=='Jersey') return true;
    else if(num == 405 && $scope.ropaSelected=='Mono') return true;
    else if(num == 406 && $scope.ropaSelected=='Pantalones') return true;
    else if(num == 407 && $scope.ropaSelected=='Polo') return true;
    else if(num == 408 && $scope.ropaSelected=='Vestido') return true;

    if(num == 500 && $scope.calzadoSelected=='Bambas') return true;
    else if(num == 501 && $scope.calzadoSelected=='Zapatos') return true;
    else if(num == 502 && $scope.calzadoSelected=='Botas') return true;
    else if(num == 503 && $scope.calzadoSelected=='Chanclas') return true;

    if(num == 600 && $scope.complementoSelected=='Bufanda') return true;
    else if(num == 601 && $scope.complementoSelected=='Gorro') return true;
    else if(num == 602 && $scope.complementoSelected=='Calcetines') return true;
    else if(num == 603 && $scope.complementoSelected=='Guantes') return true;
    else if(num == 604 && $scope.complementoSelected=='Cinturón') return true;
    else if(num == 605 && $scope.complementoSelected=='Tirantes') return true;

    if(num == 700 && $scope.temporadaSelected=='Otoño/Invierno') return true;
    else if(num == 701 && $scope.temporadaSelected=='Primavera/Verano') return true;
    else if(num == 702 && $scope.temporadaSelected=='Todo el año') return true;

    if(num == 800 && $scope.estadoSelected=='Bien') return true;
    else if(num == 801 && $scope.estadoSelected=='Muy Bien') return true;
    else if(num == 802 && $scope.estadoSelected=='Nuevo') return true;
  }
  $scope.activaCaracProduct = function(num) {
    $scope.selectProduct(num);
    //console.log("entro: ", num);
  };
  $scope.selectProduct = function(num){
    if(num == 0) $scope.generoSelected='Niño';
    else if(num == 1) $scope.generoSelected='Niña';
    else if(num == 2) $scope.generoSelected='Bebe';

    if(num == 100) $scope.edadSelected=2;
    else if(num == 101) $scope.edadSelected=3;
    else if(num == 102) $scope.edadSelected=4;
    else if(num == 103) $scope.edadSelected=5;
    else if(num == 104) $scope.edadSelected=6;
    else if(num == 105) $scope.edadSelected=7;
    else if(num == 106) $scope.edadSelected=8;
    else if(num == 107) $scope.edadSelected=9;
    else if(num == 108) $scope.edadSelected=10;
    else if(num == 109) $scope.edadSelected=11;
    else if(num == 110) $scope.edadSelected=12;
    else if(num == 111) $scope.edadSelected=13;
    else if(num == 112) $scope.edadSelected=14;

    if(num == 200) $scope.mesSelected=0;
    else if(num == 201) $scope.mesSelected=1;
    else if(num == 202) $scope.mesSelected=2;
    else if(num == 203) $scope.mesSelected=3;
    else if(num == 204) $scope.mesSelected=6;
    else if(num == 205) $scope.mesSelected=9;
    else if(num == 206) $scope.mesSelected=12;
    else if(num == 207) $scope.mesSelected=18;

    if(num == 300) $scope.articuloSelected='Ropa';
    else if(num == 301) $scope.articuloSelected='Calzado';
    else if(num == 302) $scope.articuloSelected='Accesorios';

    if(num == 400) $scope.ropaSelected='Abrigo';
    else if(num == 401) $scope.ropaSelected='Camisa';
    else if(num == 402) $scope.ropaSelected='Camiseta';
    else if(num == 403) $scope.ropaSelected='Falda';
    else if(num == 404) $scope.ropaSelected='Jersey';
    else if(num == 405) $scope.ropaSelected='Mono';
    else if(num == 406) $scope.ropaSelected='Pantalones';
    else if(num == 407) $scope.ropaSelected='Polo';
    else if(num == 408) $scope.ropaSelected='Vestido';

    if(num == 500) $scope.calzadoSelected='Bambas';
    else if(num == 501) $scope.calzadoSelected='Zapatos';
    else if(num == 502) $scope.calzadoSelected='Botas';
    else if(num == 503) $scope.calzadoSelected='Chanclas';

    if(num == 600) $scope.complementoSelected='Bufanda';
    else if(num == 601) $scope.complementoSelected='Gorro';
    else if(num == 602) $scope.complementoSelected='Calcetines';
    else if(num == 603) $scope.complementoSelected='Guantes';
    else if(num == 604) $scope.complementoSelected='Cinturón';
    else if(num == 605) $scope.complementoSelected='Tirantes';

    if(num == 700) $scope.temporadaSelected='Otoño/Invierno';
    else if(num == 701) $scope.temporadaSelected='Primavera/Verano';
    else if(num == 702) $scope.temporadaSelected='Todo el año';

    if(num == 800) $scope.estadoSelected='Bien';
    else if(num == 801) $scope.estadoSelected='Muy Bien';
    else if(num == 802) $scope.estadoSelected='Nuevo';
  }

  //////AMAZON
  $scope.sizeLimit      = 10585760; // 10MB in Bytes
  $scope.uploadProgress = 0;
  $scope.creds          = {};

  $scope.creds.access_key="AKIAIQTXU2EEQWS37R3Q";
  $scope.creds.secret_key="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";
  $scope.creds.bucket="kidsify";

  var refuser = new Firebase("https://kidsify.firebaseio.com/users").child(window.localStorage.getItem('uid'));
  $scope.user = $firebaseObject(refuser);
  var uploadLogo = function(images, keyref, ref) {
    $scope.user.numproducts++;
    var refuserproduct = new Firebase("https://kidsify.firebaseio.com/userproducts").child(window.localStorage.getItem('uid')).child('products');

    AWS.config.update({ accessKeyId: $scope.creds.access_key, secretAccessKey: $scope.creds.secret_key });
    AWS.config.region = 'us-east-1';
    var bucket = new AWS.S3({ params: { Bucket: $scope.creds.bucket } });
    //console.log(images);
    var array = [];
    angular.forEach(images,function(image, key){
      //console.log(key, image);

      if(image) {
          // Perform File Size Check First
          var fileSize = Math.round(parseInt(image.size));
          if (fileSize > $scope.sizeLimit) {
            console.error('Sorry, your attachment is too big. <br/> Maximum '  + $scope.fileSizeLabel() + ' file attachment allowed','File Too Large');
            return false;
          }
          // Prepend Unique String To Prevent Overwrites
          var uniqueFileName = "logo"+$scope.uniqueString() + '-hjk' + key;
          //alert(uniqueString);
          $scope.urlProducts = "https://kidsify.s3.amazonaws.com/products/"+keyref+"/"+uniqueFileName;
          var params = { Key: "products/"+keyref+"/"+uniqueFileName, ContentType: image.type, Body: convertDataURIToBinary('data:image/jpeg;base64,'+image), ServerSideEncryption: 'AES256' };
          //console.log(image);

          array.push($scope.urlProducts);
          // alert(array);

          bucket.putObject(params, function(err, data) {
            if(err) {
              console.error(err.message,err.code);
              return false;
            }
            else {
              setTimeout(function() {
                $scope.uploadProgress = 0;
                $scope.$digest();

              }, 2500);


                if($scope.uploadProgress==100) {
                  $cordovaGeolocation
        					.getCurrentPosition({timeout: 10000, enableHighAccuracy: false})
        					.then(function (position) {

                    if(!$scope.edadSelected) $scope.edadSelected='';
                    if(!$scope.mesSelected) $scope.mesSelected='';
                    if(!$scope.ropaSelected) $scope.ropaSelected='';
                    if(!$scope.calzadoSelected) $scope.calzadoSelected='';
                    if(!$scope.complementoSelected) $scope.complementoSelected='';

                    ref.set({
                        owner: window.localStorage.getItem('uid'),
                        title: $scope.product.name,
                        description: $scope.product.description,
                        price: $scope.product.price,
                        image: array,
                        genero: $scope.generoSelected,
                        edad: $scope.edadSelected,
                        mes: $scope.mesSelected,
                        articulo: $scope.articuloSelected,
                        ropa: $scope.ropaSelected,
                        calzado: $scope.calzadoSelected,
                        complemeto: $scope.complementoSelected,
                        temporada: $scope.temporadaSelected,
                        estado: $scope.estadoSelected,
                        lat: position.coords.latitude,
                        lng: position.coords.longitude,
                        reftime: -(new Date().getTime())
                    })
                    refuser.update({
                      numproducts: $scope.user.numproducts
                    })
                    refuserproduct.push({
                          assigneduser: '',
                          state: 'pending',
                          productkey: keyref
                    })

                    alert('imageUpdated');
                  })
                }
            }
          })
          .on('httpUploadProgress',function(progress) {
            $scope.uploadProgress = Math.round(progress.loaded / progress.total * 100);
            $scope.$digest();

          });
        }
        else {
          // No File Selected
          //toastr.error('Please select a file to upload');
          console.error('Please select a file to upload');
        }
      });
    }
    var uploadVideo = function(video, keyref, ref) {

      var refuserproduct = new Firebase("https://kidsify.firebaseio.com/userproducts").child(window.localStorage.getItem('uid')).child('products');

      AWS.config.update({ accessKeyId: $scope.creds.access_key, secretAccessKey: $scope.creds.secret_key });
      AWS.config.region = 'us-east-1';
      var bucket = new AWS.S3({ params: { Bucket: $scope.creds.bucket } });
      //console.log(images);
      var array = [];
        //console.log(key, image);
        if(video) {
            // Perform File Size Check First
            //var fileSize = Math.round(parseInt(video.size));

            // Prepend Unique String To Prevent Overwrites
            var uniqueFileName = "logo"+$scope.uniqueString() + '-hjk82';
            alert(uniqueFileName);
            //alert(uniqueString);
            $scope.urlProducts = "https://kidsify.s3.amazonaws.com/products/video/"+keyref+"/"+uniqueFileName;
            alert(video.type);
            var params = { Key: "products/video/"+keyref+"/"+uniqueFileName, ContentType: "video/mp4", Body: convertDataURIToBinary(videobase64), ServerSideEncryption: 'AES256' };

            array.push($scope.urlProducts);
            // alert(array);

            bucket.putObject(params, function(err, data) {
              if(err) {
                console.error(err.message,err.code);
                return false;
              }
              else {
                setTimeout(function() {
                  $scope.uploadProgress = 0;
                  $scope.$digest();

                }, 2500);
                if($scope.uploadProgress==100) {
                    ref.update({
                        video: array
                    })
                }
                alert('Success');
              }
            })
            .on('httpUploadProgress',function(progress) {
              $scope.uploadProgress = Math.round(progress.loaded / progress.total * 100);
              $scope.$digest();

            });
          }

          else {
            // No File Selected
            //toastr.error('Please select a file to upload');
            console.error('Please select a file to upload');
          }

      }

    var BASE64_MARKER = ';base64,';
    function convertDataURIToBinary(dataURI) {
      var base64Index = dataURI.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
      var base64 = dataURI.substring(base64Index);
      var raw = window.atob(base64);
      var rawLength = raw.length;
      var array = new Uint8Array(new ArrayBuffer(rawLength));

      for(i = 0; i < rawLength; i++) {
        array[i] = raw.charCodeAt(i);
      }
      return array;
    }

    $scope.uniqueString = function() {
      var text     = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

      for( var i=0; i < 8; i++ ) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
      }
      return text;
    }

    $scope.fileSizeLabel = function() {
    // Convert Bytes To MB
    return Math.round($scope.sizeLimit / 1024 / 1024) + 'MB';
  };
  ////////

  //Post product
  $scope.postProduct = function() {
    var refphotos = new Firebase("https://kidsify.firebaseio.com/products");
    var newrefis = refphotos.push();

    //console.log(newrefis.key());
    //alert(newrefis);
    uploadLogo($scope.images, newrefis.key(), newrefis);
    setTimeout(function(){
      uploadVideo($scope.videoData[0], newrefis.key(), newrefis);
    }, 1000);

    $state.go('app.products');
  }
})

app.controller('ProductDetailCtrl', function($scope, $state, ProductDetail, $ionicHistory, ZoomImages,
                          $ionicBackdrop, $sce, $firebaseArray, $ionicModal, $firebaseObject, $ionicSlideBoxDelegate, $ionicScrollDelegate, PartnerProfile, ChatInfo){

  $scope.product = ProductDetail.getProduct();
  $scope.trustSrc = function(src) {
    return $sce.trustAsResourceUrl(src);
  }
  if($scope.product.video) $scope.videou = $scope.product.video[0];
  $scope.thereisvideo = false;
  setTimeout(function(){
    $scope.thereisvideo = true;
  }, 1000);

  console.log($scope.product);

  var refowner = new Firebase("https://kidsify.firebaseio.com/users").child($scope.product.owner);
  $scope.owner = $firebaseObject(refowner);

  $scope.goToProfile = function(){
    PartnerProfile.setProfile($scope.owner);
    $state.go('profile');
  }

  $scope.back = function() {
    $ionicHistory.goBack();
    //$state.go('app.products');
  }
  $scope.deleteProduct = function(product) {
    var refproduct = new Firebase("https://kidsify.firebaseio.com/products").child($scope.product.$id);
    refproduct.remove();

    var refuserproducts = new Firebase("https://kidsify.firebaseio.com/userproducts").child(window.localStorage.getItem('uid')).child('products');
    $scope.userproducts = $firebaseArray(refuserproducts);
    $scope.userproducts.$loaded()
      .then(function() {
        $scope.userproducts.forEach(function(val, key){
          if(val.productkey == $scope.product.$id) refuserproducts.child(val.$id).remove();//console.log(val);
        });
      });

      var refuser = new Firebase("https://kidsify.firebaseio.com/users").child(window.localStorage.getItem('uid'));
      $scope.user = $firebaseObject(refuser);
      $scope.user.$loaded()
        .then(function() {
          $scope.user.numproducts -= 1;
          $scope.user.$save();
        });

      $ionicHistory.goBack();
  }

  $scope.isProductOwner = function() {
    if(window.localStorage.getItem('uid') && $scope.product.owner == window.localStorage.getItem('uid')) return true;
    return false;
  }
  $scope.createChat = function() {
    ChatInfo.setChatWith($scope.owner);
    $state.go('chatroom');
    //console.log('Chat between ', window.localStorage.getItem('uid'), $scope.product.owner);
    //var refchat = new Firebase("https://kidsify.firebaseio.com/userchat").child(window.localStorage.getItem('uid')).child($scope.product.owner);
  }

  //MAP LOGIC
$scope.map;
$scope.$on('mapInitialized', function(event, map){
  $scope.map = map;
})
var me_marker = {
  iconUrl: 'img/me-marker.png'
};
angular.extend($scope, {
      berlin: {
          icon: me_marker,
          lat: $scope.product.lat,
          lng: $scope.product.lng,
          zoom: 14
      },
      layers: {
          baselayers: {
              googleRoadmap: {
                  name: 'Google Streets',
                  layerType: 'ROADMAP',
                  type: 'google'
              }
          }
      },
      defaults: {
        zoomControl: false,
        attributionControl: false, //this is for Leaflet title
        controls: {
          layers: {
            visible: false //layers switch
          }
        }
      },
      paths: {
        circle: {
                    type: "circle",
                    radius: 1000,
                    latlngs: {
                      lat: $scope.product.lat,
                      lng: $scope.product.lng
                    }
                },
      },
      events: {
          map: {
              enable: ['zoomstart', 'drag', 'click', 'mousemove','moveend'],
              logic: 'emit'
          }
      }
    });
    //////////

  $scope.imageZoom = function() {
    ZoomImages.setImages($scope.product.image);
    $state.go('zoomview');
  }
  $scope.swiper;
  $scope.swiperProductDetail = {

    /* Whatever options */
    effect: 'slide',
    speed: 100,
    width: 100,
    height: 80,
    direction: 'horizontal',
    initialSlide: 0,
    /* Initialize a scope variable with the swiper */
    onInit: function(swiper){
      $scope.swiper = swiper;
    }
  };

  $scope.zoomMin = 1;
  $scope.showImages = function(index) {
    $scope.activeSlide = index;
    $scope.showModal('templates/zoomview.html');
  };
  $scope.showModal = function(templateUrl) {
    $ionicModal.fromTemplateUrl(templateUrl, {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
      $scope.modal.show();
    });
  }
  $scope.closeModal = function() {
    $scope.modal.hide();
    $scope.modal.remove();
  };

  $scope.autoZoom = function() {
    $ionicScrollDelegate.zoomBy(1.5, true);
  }

  $scope.updateSlideStatus = function(slide) {
    var zoomFactor = $ionicScrollDelegate.$getByHandle('scrollHandle' + slide).getScrollPosition().zoom;
    if (zoomFactor == $scope.zoomMin) {
      $ionicSlideBoxDelegate.enableSlide(true);
    } else {
      $ionicSlideBoxDelegate.enableSlide(false);
    }
  };

})

app.config(function($httpProvider){
  $httpProvider.defaults.headers.common = {};
  $httpProvider.defaults.headers.post = {'Content-Type': 'application/json'};
  $httpProvider.defaults.headers.put = {};
  $httpProvider.defaults.headers.patch = {};

  window.Stripe.setPublishableKey('pk_test_D7buss4G6qcCgEvLeYm0Cavg');
  //window.Stripe.setPublishableKey('pk_live_E8ZfqkoXfbhKNYbVVL4e7T1x');
})
app.controller('PremiumCtrl', function($scope, $state, $http, $firebaseObject, $firebaseArray, $ionicLoading, $timeout, $ionicHistory, ChatInfo) {

  $scope.$on('$ionicView.beforeEnter', function() {
    $ionicLoading.show({
      template: 'Estableciendo conexión segura con Stripe...'
    });

    var profileRef = new Firebase("https://kidsify.firebaseio.com/users").child(window.localStorage.getItem('uid'));
    $scope.profile = $firebaseObject(profileRef);
    $scope.profile.$loaded()
      .then(function() {
        //CHECK IF PREMIUM
        if($scope.profile.stripecustomerid){
            var retrievedata = ({
                customerid: $scope.profile.stripecustomerid,
                subscriptionid: $scope.profile.stripesubsid
            });

            //$http.post('http://localhost:8000/'+'testingretrievekidsify', JSON.stringify(retrievedata)).
            $http.post('https://server-jordi.herokuapp.com/'+'testingretrievekidsify', JSON.stringify(retrievedata)).
            success(function(data) {
              console.log('Success: ', data);
              $scope.isSubscribed = true;
              $ionicLoading.hide();
            })
            .error(function(data) {
              console.log('Error: ', data);
              $scope.isSubscribed = false;
              $ionicLoading.hide();
            });
        }
        else {
          $scope.isSubscribed = false;
          $ionicLoading.hide();
        }
      });
  });

  //STRIPE PAYMENT
  $scope.couponData = {};
  $scope.creds = {};

  $scope.finishedBuying = false;
  $scope.stripeCallback = function (code, result) {
    $scope.finishedBuying = true;

    if (result.error) {
      //console.log(result.error);
      alert('Error');
      $scope.finishedBuying = false;
    }
    else {
      //if(!$scope.savedcreditcard) $scope.sendStripeInfo(result);
      //if($scope.savedcreditcard) $scope.sendStripeInfoAndSave(result);
      $scope.sendStripeInfo(result);
    }
  };

  $scope.sendStripeInfo = function (result) {
        $ionicLoading.show({
          template: 'Realizando el pago...'
        });

        var pepe = ({
            token: result,
            receipt_email: $scope.profile.email
        });

        //$http.post('http://localhost:8000/'+'testingpaymentkidsify', JSON.stringify(pepe)).
        $http.post('https://server-jordi.herokuapp.com/'+'testingpaymentkidsify', JSON.stringify(pepe)).
        success(function(data) {

            //Firebase update
            if(data.valid == 'tarjeta valida'){
              //console.log(data.customer.id, data.customer.subscriptions.data[0].id);
              $scope.profile.stripecustomerid = data.customer.id;
              $scope.profile.stripesubsid = data.customer.subscriptions.data[0].id;

              $scope.profile.$save();
              $ionicLoading.hide();
              $scope.finishedBuying = true;
            }
            else alert('ERROR 1');
        })
        .error(function(data) {
          $ionicLoading.hide();
          alert('ERROR 2');
          $scope.finishedBuying = false;
        });
    };

    $scope.cancelSubs = function() {
      var canceldata = ({
          customerid: $scope.profile.stripecustomerid,
          subscriptionid: $scope.profile.stripesubsid
      });

      //$http.post('http://localhost:8000/'+'testingcancelkidsify', JSON.stringify(canceldata)).
      $http.post('https://server-jordi.herokuapp.com/'+'testingcancelkidsify', JSON.stringify(canceldata)).
      success(function(data) {
        console.log('Success: ', data);
        //alert(data.current_period_end);
      })
      .error(function(data) {
        console.log('Error: ', data);

      });
    }

})

app.controller('MessagesCtrl', function($scope, $state, $firebaseObject, $firebaseArray, $timeout, $ionicHistory, ChatInfo) {
  var userchatRef = new Firebase("https://kidsify.firebaseio.com/userchat").child(window.localStorage.getItem('uid')).child('chats');
  var users = new Firebase("https://kidsify.firebaseio.com/users");
  $scope.users = $firebaseArray(users);
  $scope.messages = $firebaseArray(userchatRef);

  $scope.flagbeforeenter = true;

  $scope.$on('$ionicView.beforeEnter', function() {
    //console.log('BEFORE ENTER');

      users = new Firebase("https://kidsify.firebaseio.com/users");
      $scope.users = $firebaseArray(users);

      userchatRef = new Firebase("https://kidsify.firebaseio.com/userchat").child(window.localStorage.getItem('uid')).child('chats');
      $scope.messages = $firebaseArray(userchatRef);

      $scope.usersintheinbox = [];

      $scope.generalsms = [];
      $scope.userscount = [];
      var statuscount = 0;
      $scope.messages.$loaded()
        .then(function() {
          $scope.users.$loaded()
            .then(function() {
              //console.log($scope.messages);
              $scope.messages.forEach(function(valmessages, keymessages){
                //console.log('FIND');
                    $scope.users.forEach(function(valusers, keyusers){
                      //console.log(valusers.$id , valmessages.$id);
                        if(valusers.$id == valmessages.$id) {
                          ////////
                          var chatref = new Firebase("https://kidsify.firebaseio.com/chats").child(valmessages.key).child('messages');
                          var chatuserref = new Firebase("https://kidsify.firebaseio.com/chats").child(valmessages.key);

                          var usercount = $firebaseObject(chatuserref);
                          $scope.imtheuser = false;
                          usercount.$loaded()
                            .then(function() {
                              //console.log($scope.usercount.ownerID, window.localStorage.getItem('uid'));
                              if(usercount.ownerID == window.localStorage.getItem('uid')) $scope.imtheuser = true;
                              else $scope.imtheuser = false;

                              $scope.userscount.push(usercount);

                            });

                          var lastmessagesusers = [];
                          lastmessagesusers = $firebaseArray(chatref);
                          lastmessagesusers.$loaded()
                            .then(function() {
                                $scope.usersintheinbox.push(valusers);
                                $scope.generalsms.push(lastmessagesusers);
                            });
                        }
                    });
                });
            });
        });
    });

    $scope.$on('$ionicView.enter', function() {
      //console.log('ENTER');
      $scope.shouldShowDelete = false;

      $timeout(function() {
        //console.log('TIMEOUT');
         $scope.flagbeforeenter = false;
      }, 200);
    });

    userchatRef.on('child_added', function(childSnapshot, prevChildKey) {
      if(!$scope.flagbeforeenter) {

          //console.log('child_added', childSnapshot, prevChildKey);

          $scope.usersintheinbox = [];

          $scope.generalsms = [];
          $scope.userscount = [];
          var statuscount = 0;
          $scope.messages.$loaded()
            .then(function() {
              $scope.users.$loaded()
                .then(function() {
                  //console.log($scope.messages);
                  $scope.messages.forEach(function(valmessages, keymessages){
                    //console.log('FIND');
                        $scope.users.forEach(function(valusers, keyusers){
                          //console.log(valusers.$id , valmessages.$id);
                            if(valusers.$id == valmessages.$id) {
                              ////////
                              var chatref = new Firebase("https://kidsify.firebaseio.com/chats").child(valmessages.key).child('messages');
                              var chatuserref = new Firebase("https://kidsify.firebaseio.com/chats").child(valmessages.key);
                              $scope.usercount = $firebaseObject(chatuserref);
                              $scope.imtheuser = false;
                              $scope.usercount.$loaded()
                                .then(function() {
                                  //console.log($scope.usercount.ownerID, window.localStorage.getItem('uid'));
                                  if($scope.usercount.ownerID == window.localStorage.getItem('uid')) $scope.imtheuser = true;
                                  else $scope.imtheuser = false;
                                  //console.log($scope.usercount);
                                  $scope.userscount.push($scope.usercount);
                                });

                              var lastmessagesusers = [];
                              lastmessagesusers = $firebaseArray(chatref);
                              lastmessagesusers.$loaded()
                                .then(function() {
                                    $scope.usersintheinbox.push(valusers);
                                    $scope.generalsms.push(lastmessagesusers);
                                });
                            }
                        });
                    });
                });
            });
          }
    });

    $scope.goToChat = function(user) {
      ChatInfo.setChatWith(user);
      $state.go('chatroom');
    }

    $scope.borrarUser = function(user) {

    }

    $scope.deleteUserMessages = function(user) {
      console.log(user.$id);
    }

    $scope.edit = function() {
      if($scope.shouldShowDelete) $scope.shouldShowDelete = false;
      else $scope.shouldShowDelete = true;
    }

})

app.controller('ChatRoomCtrl', function($scope, $state, $firebaseObject, $firebaseArray, $cordovaKeyboard, $ionicScrollDelegate, $timeout, $ionicHistory, ChatInfo, $ionicModal) {
  $scope.back = function() {
    $ionicHistory.goBack();
    //$state.go('app.products');
  }

  var viewScroll = $ionicScrollDelegate.$getByHandle('userMessageScroll');
  var footerBar; // gets set in $ionicView.enter
  var scroller;
  var txtInput; // ^^^

  var messagesRef;
  // var userchatRef;
  // var chatRef;

  var userchatRef;// = new Firebase("https://kidsify.firebaseio.com/userchat").child(window.localStorage.getItem('uid')).child('chats').child(ChatInfo.getChatWith().$id);
  var chatRef;// = new Firebase("https://kidsify.firebaseio.com/chats");
  var messagesAddChildRef;
  $scope.candidateref = '';
  $scope.nummessages = 0;
  $scope.messages = [];

  $scope.$on('$ionicView.beforeEnter', function() {
    //$scope.enemyID = ChatInfo.getChatWith();
    // if (window.cordova && window.cordova.plugins.Keyboard) {
    //   cordova.plugins.Keyboard.disableScroll(true);
    //   window.addEventListener('native.keyboardshow', keyboardShowHandlerUGO);
    //   window.addEventListener('native.keyboardhide', keyboardHideHandlerUGO);
    //

    if($cordovaKeyboard) $cordovaKeyboard.disableScroll(true);
    window.addEventListener('native.keyboardshow', keyboardShowHandlerUGO);
    window.addEventListener('native.keyboardhide', keyboardHideHandlerUGO);
    window.addEventListener('native.keyboardshow', function(){
     document.body.classList.add('keyboard-open');
   });

     //plus anything else you need to do
     //getMessages();
    //  messagesRef = new Firebase("https://rutadelbonpa.firebaseio.com/taskchat").child(chatInfo.getTaskRef()).child(chatInfo.getChatWith()).child('messages');
    var enemyRef = new Firebase("https://kidsify.firebaseio.com/users").child(ChatInfo.getChatWith().$id);
    $scope.enemypreID = $firebaseObject(enemyRef);
    $scope.enemypreID.$loaded()
      .then(function() {
        $scope.enemyID = $scope.enemypreID;
      });

    userchatRef = new Firebase("https://kidsify.firebaseio.com/userchat").child(window.localStorage.getItem('uid')).child('chats').child(ChatInfo.getChatWith().$id);
    chatRef = new Firebase("https://kidsify.firebaseio.com/chats");

    $scope.shouldapplychildadded = false;
    setTimeout(function(){
      $scope.shouldapplychildadded = true;
    },1000);

    $scope.chatidfireobject = $firebaseObject(userchatRef);
    $scope.chatidfireobject.$loaded()
      .then(function() {
          if($scope.chatidfireobject.key) {
               //console.log('EXISTE CHAT CUANDO ENTRO with', ChatInfo.getChatWith().$id);
               $scope.chatid = $scope.chatidfireobject.key;
               $scope.thischatownerID = $firebaseObject(chatRef.child($scope.chatid).child('ownerID'));
               $scope.thischatstatuscountuser = $firebaseObject(chatRef.child($scope.chatid).child('statuscountuser'));
               $scope.thischatstatuscountenemy = $firebaseObject(chatRef.child($scope.chatid).child('statuscountenemy'));
               messagesAddChildRef = chatRef.child($scope.chatid).child('messages');
               $scope.messages = $firebaseArray(chatRef.child($scope.chatid).child('messages'));
               $scope.messages.$loaded()
                 .then(function() {
                     $scope.messages.forEach(function(val, key) {
                        if(val.userid != window.localStorage.getItem('uid')) {
                          val.status = 'checked';
                          $scope.messages.$save(val);
                        }
                     });
                 });

                 $scope.thischatstatuscountuser.$loaded()
                   .then(function() {
                     $scope.thischatstatuscountenemy.$loaded()
                       .then(function() {
                           if(window.localStorage.getItem('uid') != $scope.thischatownerID.$value) {
                             $scope.thischatstatuscountenemy.$value = 0;
                             $scope.thischatstatuscountenemy.$save();
                           }
                           else {
                             $scope.thischatstatuscountuser.$value = 0;
                             $scope.thischatstatuscountuser.$save();
                           }
                       });
                   });

                   //child_added to conversation
                   if(messagesAddChildRef){
                     messagesAddChildRef.on('child_added', function(childSnapshot, prevChildKey) {
                       if($scope.shouldapplychildadded){
                         //console.log('child_added', childSnapshot.val(), prevChildKey);
                            if(childSnapshot.val().userid != window.localStorage.getItem('uid')) {
                                var chatminRef = new Firebase("https://kidsify.firebaseio.com/chats/"+$scope.chatid+"/messages/"+childSnapshot.key());
                                var chatminObj = $firebaseObject(chatminRef);
                                chatminObj.$loaded().then(function(){
                                  setTimeout(function(){
                                    chatminRef.update({
                                      status: 'checked'
                                    });
                                  },500);
                                });
                            }

                             $scope.thischatstatuscountuser.$loaded()
                               .then(function() {
                                 $scope.thischatstatuscountenemy.$loaded()
                                   .then(function() {
                                     var chatminReff = new Firebase("https://kidsify.firebaseio.com/chats/"+$scope.chatid);
                                     var chatminObjj = $firebaseObject(chatminReff);
                                     chatminObjj.$loaded().then(function(){
                                         if(window.localStorage.getItem('uid') != $scope.thischatownerID.$value) {
                                           setTimeout(function(){
                                             chatminReff.update({
                                               statuscountenemy: 0
                                             });
                                           },500);
                                         }
                                         else {
                                           setTimeout(function(){
                                             chatminReff.update({
                                               statuscountuser: 0
                                             });
                                           },500);
                                         }
                                     });

                                 });
                             });
                           }
                     });

                   }
           }
           else {
             //console.log('NO EXISTE');
           }
    });

     $scope.candidateref = messagesRef;


     //MESSAGES
     $scope.nummessages=0;
     //$scope.messages = $firebaseArray(messagesRef);

      $timeout(function() {
         //viewScroll = $ionicScrollDelegate.$getByHandle('userMessageScroll');
         footerBar = document.body.querySelector('#userMessagesView .bar-footer');
         scroller = document.body.querySelector('#userMessagesView .scroll-content');
         txtInput = angular.element(footerBar.querySelector('textarea'));
      }, 0);

  });

  //chatRef.child($scope.chatid).child('messages')

  $scope.$on('$ionicView.beforeLeave', function() {
      //cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);


      // if (window.cordova && window.cordova.plugins.Keyboard) {
      //   window.removeEventListener('native.keyboardshow', keyboardShowHandlerUGO);
      //   window.removeEventListener('native.keyboardhide', keyboardHideHandlerUGO);
      //   cordova.plugins.Keyboard.disableScroll(false);
      // }

      window.removeEventListener('native.keyboardshow', keyboardShowHandlerUGO);
      window.removeEventListener('native.keyboardhide', keyboardHideHandlerUGO);
      if($cordovaKeyboard) $cordovaKeyboard.disableScroll(false);

      $scope.enemyID = '';

      //if(messagesAddChildRef) {
          //console.log('delete child_added');
          messagesAddChildRef.off('child_added');
      //}
  });
  $scope.checkNewMessage = function() {
    if($scope.messages.length > $scope.nummessages){
      $scope.nummessages=$scope.messages.length;
      $timeout(function() {
        viewScroll.scrollBottom();
      }, 0);
    }
  };
  $scope.message='';
  $scope.inputmessage='';

  $scope.userID=window.localStorage.getItem('uid');

  //src images
  $scope.enemyID = ChatInfo.getChatWith();

  function keepKeyboardOpenUGO() {
    //console.log('keepKeyboardOpen');
    txtInput.one('blur', function() {
      //console.log('textarea blur, focus back on it');
      txtInput[0].focus();
    });
  }

  Date.prototype.hhmm = function() {
      var newDateObj = new Date(this.getTime());
      var h = newDateObj.getHours().toString();
      var min = newDateObj.getMinutes().toString();
      return (h[1]?h:"0"+h[0]) +':'+ (min[1]?min:"0"+min[0]); // padding
  };
  $scope.sendMessage = function(sendMessageForm) {

      // var statuscount = 0;
      // //////////
      // lastmessagesusers.forEach(function(valmessagesdet, keymessagesdet) {
      //     if(valmessagesdet.status == 'pending' && valmessagesdet.userid != window.localStorage.getItem('uid')){
      //       console.log(valmessagesdet);
      //       ++statuscount;
      //     }
      // });
      // console.log(statuscount);
      // lastmessagesusers.statuscount = statuscount;
      //$scope.usersintheinbox.push(valusers);
      //$scope.generalsms.push(lastmessagesusers);
      //////////


      //console.log('SEND SMS');
      $scope.message = $scope.inputmessage;
      keepKeyboardOpenUGO();

      //console.log('INPUT: ', $scope.inputmessage);
      if(!$scope.chatid){
          //console.log('No existe el chat');

          var newchatref = chatRef.push();
          userchatRef.set({
            'key': newchatref.key()
          });

          //get the other user ref
          var otheruserchatRef = new Firebase("https://kidsify.firebaseio.com/userchat").child(ChatInfo.getChatWith().$id).child('chats').child(window.localStorage.getItem('uid'));
          otheruserchatRef.set({
            key: newchatref.key()
          });

          //console.log('BEFORE HOUR');
          $scope.messagehourmin = new Date().hhmm();
          //console.log($scope.messagehourmin);
          chatRef.child(newchatref.key()).child('messages').push({
              message: $scope.message,
              status: 'pending',
              reftime: -new Date(),
              datehourmin: $scope.messagehourmin,
              userid: window.localStorage.getItem('uid')
          });

          // $scope.objectcountnew = $firebaseObject(chatRef.child(newchatref.key()));
          // $scope.objectcountnew.$loaded()
          //   .then(function() {
          //       $scope.objectcountnew.ownerID = window.localStorage.getItem('uid');
          //       $scope.objectcountnew.enemyID = ChatInfo.getChatWith().$id;
          //       $scope.objectcountnew.statuscountenemy++;
          //       $scope.objectcountnew.statuscountuser = 0;
          //       $scope.objectcountnew.$save();
          //     });
          chatRef.child(newchatref.key()).update({
              ownerID: window.localStorage.getItem('uid'),
              enemyID: ChatInfo.getChatWith().$id,
              statuscountenemy: 1,
              statuscountuser: 0
          });

          $scope.chatid = newchatref.key();
          $scope.messages = $firebaseArray(chatRef.child(newchatref.key()).child('messages'));
    }
    else {
          //console.log('Existe el chat');
          //console.log('BEFORE HOUR');
          $scope.messagehourmin = new Date().hhmm();
          //console.log($scope.messagehourmin);
          chatRef.child($scope.chatid).child('messages').push({
              message: $scope.message,
              status: 'pending',
              reftime: -new Date(),
              datehourmin: $scope.messagehourmin,
              userid: window.localStorage.getItem('uid')
          });

          $scope.objectcount = $firebaseObject(chatRef.child($scope.chatid));
          $scope.objectcount.$loaded()
            .then(function() {
                  if(window.localStorage.getItem('uid') == $scope.objectcount.ownerID){
                      //console.log($scope.objectcount);
                      $scope.objectcount.statuscountenemy++;
                      $scope.objectcount.$save();
                  }
                  else {
                    //console.log($scope.objectcount);
                    $scope.objectcount.statuscountuser++;
                    $scope.objectcount.$save();
                  }
              });
    }

    $scope.nummessages++;

    //PUSH Notification
    // var refuser = new Firebase("https://rutadelbonpa.firebaseio.com/users").child($scope.enemyID).child('pushUid');
    // $scope.userdevices = $firebaseArray(refuser);
    // $scope.userdevices.$loaded()
    //   .then(function() {
    //     //console.log($scope.userdevices, $scope.userdevices.length);
    //     $scope.idstopush = [];
    //     $scope.userdevices.forEach(function(val, key){
    //       //console.log(val.id);
    //       $scope.idstopush.push(val.id);
    //     });
    //
    //
    //     document.addEventListener('deviceready', function () {
    //       //alert('TRYING TO PUSH');
    //         var notificationObj = {
    //                                 "app_id": "48f3b771-185b-41bd-bce2-fa2dc44b2971",
    //                                 "include_player_ids": $scope.idstopush,
    //                                 "data": {"foo": "bar"},
    //                                 "ios_badgeCount": 1,
    //                                 "ios_badgeType": "Increase",
    //                                 "contents": {"en": $scope.message},
    //                                 "headings": {"en": "Chat"},
    //                               };
    //         window.plugins.OneSignal.postNotification(notificationObj,
    //           function(successResponse) {
    //             //alert('NICE');
    //           },
    //           function (failedResponse) {
    //             //alert('FAILED');
    //           }
    //         );
    //     });
    //   });


    $timeout(function() {
      viewScroll.scrollBottom(true, true);
      keepKeyboardOpenUGO();
    }, 0);

    keepKeyboardOpenUGO();
    keepKeyboardOpenUGO();

    $scope.inputmessage='';

    ////see image profile
    $scope.olakase = function() {
      console.log('ola k ase');
    }
    //
  }

  //initial Scroll
  $timeout(function() {
    viewScroll.scrollBottom();
  }, 0);

  var keyboardHeightUGO = 0;
  function keyboardShowHandlerUGO(e) {

    //scroller.style.bottom = e.keyboardHeight + 'px';
    //viewScroll.scrollBottom();

    console.log('Keyboard height is: ' + e.keyboardHeight);
    keyboardHeightUGO = e.keyboardHeight;
    //keepKeyboardOpenUGO();
    $timeout(function() {
      viewScroll.scrollBottom(true, true); // read below on this
      keepKeyboardOpenUGO();
    }, 0);

  }

  $scope.closeKeyboard = function () {
    // console.log('close');
    // console.log('Goodnight, sweet prince');
    //
    ionic.DomUtil.blurAll();

    keyboardHeightUGO = 0;
    $timeout(function() {
      scroller.style.bottom = footerBar.clientHeight + 'px';
      ionic.DomUtil.blurAll();
      viewScroll.scrollBottom(true, false);
    }, 0);
    //
    // cordova.plugins.Keyboard.close();

  }

  function keyboardHideHandlerUGO(e) {
      console.log('Goodnight, sweet prince');
  }

  $scope.onMessageHold = function(e, itemIndex, message) {
     console.log('onMessageHold');
     console.log('message: ' + JSON.stringify(message, null, 2));
     $ionicActionSheet.show({
       buttons: [{
         text: 'Copy Text'
       }, {
         text: 'Delete Message'
       }],
       buttonClicked: function(index) {
         switch (index) {
           case 0: // Copy Text
             if (window.cordova && window.cordova.plugins) cordova.plugins.clipboard.copy(message.text);

             break;
           case 1: // Delete
             // no server side secrets here :~)
             $scope.messages.splice(itemIndex, 1);
             $timeout(function() {
               viewScroll.resize();
             }, 0);

             break;
         }

         return true;
       }
     });
   };

   $scope.$on('elastic:resize', function(e, ta) {
    console.log('taResize');
    if (!ta) return;

    var taHeight = ta[0].offsetHeight;
    console.log('taHeight: ' + taHeight);

    if (!footerBar) return;

    var newFooterHeight = taHeight + 10;
    newFooterHeight = (newFooterHeight > 44) ? newFooterHeight : 44;

    footerBar.style.height = newFooterHeight + 'px';

    // for iOS you will need to add the keyboardHeight to the scroller.style.bottom
    if (device.platform.toLowerCase() === 'ios') {
      console.log('keyboardHeightUGO: ', keyboardHeightUGO);
      scroller.style.bottom = newFooterHeight + keyboardHeightUGO + 'px';
    } else {
      scroller.style.bottom = newFooterHeight + 'px';
    }
  });

})

app.controller('WelcomeCtrl', function($scope, $state, $firebaseArray, $timeout) {

  $scope.activateAnimation = function() {
    $state.go('app.products');
  }
})
app.controller('MyProfileCtrl', function($scope, $state, $ionicHistory, $firebaseObject, $firebaseArray, $ionicModal, $timeout, ProductDetail) {

  var refuser;// = new Firebase("https://kidsify.firebaseio.com/users").child(window.localStorage.getItem('uid'));
  //$scope.user = $firebaseObject(refuser);

  var refproducts;// = new Firebase("https://kidsify.firebaseio.com/products");
  //$scope.products = $firebaseArray(refproducts);

  var refuserproducts;// = new Firebase("https://kidsify.firebaseio.com/userproducts").child(window.localStorage.getItem('uid')).child('products');
  //$scope.userproducts = $firebaseArray(refuserproducts);
  //$scope.showuserproducts = [];

  $scope.$on('$ionicView.beforeEnter', function() {
    $scope.user = '';
    $scope.products = '';
    $scope.userproducts = '';
    $scope.showuserproducts = [];
    $timeout(function() {});

    refuser = new Firebase("https://kidsify.firebaseio.com/users").child(window.localStorage.getItem('uid'));
    $scope.user = $firebaseObject(refuser);

    refproducts = new Firebase("https://kidsify.firebaseio.com/products");
    $scope.products = $firebaseArray(refproducts);

    refuserproducts = new Firebase("https://kidsify.firebaseio.com/userproducts").child(window.localStorage.getItem('uid')).child('products');
    $scope.userproducts = $firebaseArray(refuserproducts);

    //console.log('Enter');
    $scope.userproducts.$loaded().then(function() {
      $scope.showuserproducts = [];
      angular.forEach($scope.userproducts, function(value, key) {
        //console.log('primer for');
        angular.forEach($scope.products, function(valueproducts, keyproducts) {
          //console.log('segundo for', valueproducts.$id, value.productkey);
          if(valueproducts.$id == value.productkey) {
            //console.log('MATCH');
            valueproducts.key = value.productkey;
            /* HERE IS WHERE YOU CAN CHECK FOR STATUS OF EACH PRODUCT*/
            $scope.showuserproducts.push(valueproducts);
          }
        });
      });

      $scope.showuserproducts.reverse();
      $timeout(function(){});
    })
  })
  $scope.backToMenu = function(){
    console.log('back menu');
    $state.go('app.products');
    //$ionicHistory.goBack();
  }
  $scope.showEditProfileModal = function() {
    $ionicModal.fromTemplateUrl('templates/editprofile.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
      $scope.modal.show();
    });
  }

  //Modal edit profile logic
  $scope.closeEditProfile = function() {
    $scope.modal.hide();
    $scope.modal.remove();
  }
  //

  $scope.goToProductDetail = function(product) {
    ProductDetail.setProduct(product);

    $state.go('productdetail');
  }

})
app.controller('ProfileCtrl', function($scope, $state, $ionicHistory, $firebaseObject, $firebaseArray, $ionicModal, ProductDetail, PartnerProfile) {

  $scope.owner = PartnerProfile.getProfile();
  console.log($scope.owner);

  var refuser = new Firebase("https://kidsify.firebaseio.com/users").child($scope.owner.$id);
  $scope.user = $firebaseObject(refuser);

  var refproducts = new Firebase("https://kidsify.firebaseio.com/products");
  $scope.products = $firebaseArray(refproducts);

  var refuserproducts = new Firebase("https://kidsify.firebaseio.com/userproducts").child($scope.owner.$id).child('products');
  $scope.userproducts = $firebaseArray(refuserproducts);
  $scope.showuserproducts = [];

  $scope.$on('$ionicView.beforeEnter', function() {
    console.log('Enter');
    $scope.userproducts.$loaded().then(function() {
      $scope.showuserproducts = [];
      angular.forEach($scope.userproducts, function(value, key) {
        console.log('primer for');
        angular.forEach($scope.products, function(valueproducts, keyproducts) {
          console.log('segundo for', valueproducts.$id, value.productkey);
          if(valueproducts.$id == value.productkey) {
            console.log('MATCH');
            valueproducts.key = value.productkey;
            /* HERE IS WHERE YOU CAN CHECK FOR STATUS OF EACH PRODUCT*/
            $scope.showuserproducts.push(valueproducts);
          }
        });
      });

      $scope.showuserproducts.reverse();
    })
  })

  $scope.backToMenu = function(){
    console.log('back menu');
    $state.go('app.products');
    //$ionicHistory.goBack();
  }

  $scope.goToProductDetail = function(product) {
    ProductDetail.setProduct(product);

    $state.go('productdetail');
  }
})

app.factory("ZoomImages", [function(){
var products=[];
return {
    getImages : function () {
        return products; //we need some way to access actual variable value
    },
    setImages:function(newValue){
      products=newValue;
      return products;
    }
}
}])
app.factory("PartnerProfile", [function(){
var partner='';
return {
    getProfile : function () {
        return partner; //we need some way to access actual variable value
    },
    setProfile:function(newValue){
      partner=newValue;
      return partner;
    }
}
}])
app.factory("ChatInfo", [function(){
var chatuser='';
return {
    getChatWith : function () {
        return chatuser; //we need some way to access actual variable value
    },
    setChatWith:function(newValue){
      chatuser=newValue;
      return chatuser;
    }
}
}])
app.factory("ProductDetail", [function(){
var product='';
return {
    getProduct : function () {
        return product; //we need some way to access actual variable value
    },
    setProduct:function(newValue){
      product=newValue;
      return product;
    }
}
}]);
