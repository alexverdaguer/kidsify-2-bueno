// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
var app = angular.module('starter', ['ionic', 'firebase', 'starter.controllers', 'ngCordova', 'ya.nouislider', 'leaflet-directive', 'monospaced.elastic', 'angularPayments'])

app.run(function($ionicPlatform, $cordovaStatusbar) {

  $ionicPlatform.ready(function() {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
         cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
         cordova.plugins.Keyboard.disableScroll(false);
      }

      //$cordovaKeyboard.hideAccessoryBar(true);
      //$cordovaKeyboard.disableScroll(false);

      //StatusBar.overlaysWebView(true);
      //StatusBar.backgroundColorByName("red");

      document.addEventListener('deviceready', function () {
      // Enable to debug issues.
      // window.plugins.OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});

      var notificationOpenedCallback = function(jsonData) {
        console.log('didReceiveRemoteNotificationCallBack: ' + JSON.stringify(jsonData));
      };

      window.plugins.OneSignal.init("2a815b82-ba36-4ae2-98cf-038efc46f734",
                                     {googleProjectNumber: "281468059578"},
                                     notificationOpenedCallback);

      // Show an alert box if a notification comes in when the user is in your app.
      window.plugins.OneSignal.enableInAppAlertNotification(true);
    }, false);

  });
})

app.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
  //if(ionic.Platform.isAndroid()) $ionicConfigProvider.scrolling.jsScrolling(false);

  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })
  .state('login', {
    url: '/login',
    templateUrl: 'templates/login.html',
    controller: 'LoginCtrl'
  })
  .state('register', {
    url: '/register',
    templateUrl: 'templates/register.html',
    controller: 'LoginCtrl'
    })
  .state('welcome', {
    url: '/welcome',
    templateUrl: 'templates/welcome.html',
    controller: 'WelcomeCtrl'
  })
  .state('app.createproduct', {
    url: '/createproduct',
    views: {
      'menuContent': {
        templateUrl: 'templates/createproduct.html',
        controller: 'CreateProductCtrl'
      }
    }
  })
  .state('app.messages', {
    url: '/messages',
    views: {
      'menuContent': {
        templateUrl: 'templates/messages.html',
        controller: 'MessagesCtrl'
      }
    }
  })
  .state('app.premium', {
    url: '/premium',
    views: {
      'menuContent': {
        templateUrl: 'templates/premium.html',
        controller: 'PremiumCtrl'
      }
    }
  })
  .state('app.notifications', {
    url: '/notifications',
    views: {
      'menuContent': {
        templateUrl: 'templates/notifications.html'
      }
    }
  })
  .state('app.share', {
    url: '/share',
    views: {
      'menuContent': {
        templateUrl: 'templates/share.html'
      }
    }
  })
  .state('app.help', {
    url: '/help',
    views: {
      'menuContent': {
        templateUrl: 'templates/help.html'
      }
    }
  })
  .state('productdetail', {
    url: '/productdetail',
    templateUrl: 'templates/productdetail.html',
    controller: 'ProductDetailCtrl',
    cache: false
  })
  .state('myprofile', {
    url: '/myprofile',
    templateUrl: 'templates/myprofile.html',
    controller: 'MyProfileCtrl'
  })
  .state('profile', {
    url: '/profile',
    templateUrl: 'templates/profile.html',
    controller: 'ProfileCtrl'
  })
  .state('chatroom', {
    url: '/chatroom',
    templateUrl: 'templates/chatroom.html',
    controller: 'ChatRoomCtrl'
  })
  .state('app.products', {
    url: '/products',
    views: {
      'menuContent': {
        templateUrl: 'templates/products/products.html'
        //controller: 'ProductsCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback

  if(window.localStorage['uid']) $urlRouterProvider.otherwise('/app/products');
  else $urlRouterProvider.otherwise('/login');
  // $urlRouterProvider.otherwise('/app/products');
});
